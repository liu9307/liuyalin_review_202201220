import os
import time
import unittest
from myfun import *
import HTMLTestRunner


class TestMyFun(unittest.TestCase):
    # TestCase基类方法，所有case执行之前自动执行
    @classmethod
    def setUpClass(cls):
        print('这里是所有测试用例执行前的准备工作')

    # TestCase基类方法，所有case执行之后自动执行
    @classmethod
    def tearDownClass(cls):
        print('这里是所有测试用例执行后的清理工作')

    # TestCase基类方法，每次执行case前自动执行
    def setUp(self):
        print('这里是一个测试用例执行前的准备工作')

    # TestCase基类方法，每次执行case后自动执行
    def tearDown(self):
        print('这里是一个测试用例执行后的清理工作')

    @unittest.skip('我想临时跳过这个测试用例')
    def test_add(self):
        self.assertEqual(3, add(1, 2))

    def test_minus(self):
        self.skipTest('跳过这个测试用例')
        self.assertEqual(1, minus(2, 3))

    def test_multi(self):
        self.assertEqual(6, multi(2, 3))

    def test_divide(self):
        self.assertEqual(2, divide(6, 3))
        self.assertEqual(2.5, divide(5, 2))


if __name__ == '__main__':
    unittest.main(verbosity=2)
    suit=unittest.TestSuite('test_multi')
    cur_time = time.strftime('%Y_%m_%d_%H_%M_%S',time.localtime())
    pro_dir = os.path.dirname(os.path.dirname(__file__))
    report_dir = pro_dir + '//testReport//'
    report = report_dir +cur_time +'result.html'
    print(report)
    with open(report,'wb') as fp:
        runner = HTMLTestRunner.HTMLTestReportCN(stream=fp, verbosity=1, title='接口自动化测试报告', description='测试报告详情')
        runner.run(suit)