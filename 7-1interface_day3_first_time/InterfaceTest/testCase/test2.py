"""
TestSuite 用来控制多个测试用例和多个测试文件之间的测试顺序。（这里的示例中的几个测试方法并没有一定关系，但之后你写的用例可能会有先后关系，需要先执行方法 A，再执行方法 B），我们添加到 TestSuite 中的 case 是会按照添加的顺序执行的。
1．一、（1页）创建测试套件 TestSuite()
语法：
1.实例化
2.调用 addTest 方法

TestSuite()用法：
1--- suite = unittest.TestSuite()   # 实例化 Testsuite 类
注：TestSuite()主要用来创建测试套件集合
2--- suite.addTest(TestCount('test_add'))
suite.addTest(文件名.类名(‘类中的方法名’))
#调用 TestSuite 类中的 addTest：suite.addTest(类名(‘类中的方法名’))
执行顺序是按照添加的顺序执行，先添加的先执行
注：test_add 是 unittest 脚本中的被测方法，如果是添加被导入模块中的方法，则需
要指定哪个文件名,例：用例模块划分：all_test.py
suite.addTest(文件名.类名(‘类中的方法名’))

2．二、（3页）运行测试套件 TextTestRunner()
语法：
1.实例化
2.调用 run 方法
1----runner = unittest.TextTestRunner()   # 实例化 TextTestRunner 类
2----runner.run(suite)   # 调用 Testsuite 类中的 addTest 方法
注：TextTestRunner()主要用来运行测试套件
功能描述：使用 Unittest 做单元测试，addTest（）单个 case 的时候却执行全部的 case
首先造成这个结果的原因是 pycharm 配置问题。
解决方法：点pycharm菜单栏中的Run，选择Run，点要执行的文件。
编写人：liuyalin
编写时间：2023/1/24 17:23
实现逻辑：
导包
1. 
2. 
3. 
"""
import myfun
import unittest
class TestFun(unittest.TestCase):
    # def setUpClass(cls):
    #     print('这里是所有测试用例执行前的准备工作')
    def setUp(self):
        self.a=20
        self.b=10
    def test_add(self):
        result=self.a+self.b
        # result=myfun.add(self.a+self.b)
        self.assertEqual(result,30)
    def test_sub(self):
        result=self.a-self.b
        self.assertEqual(result,10)
if __name__=='__main__':
    suite=unittest.TestSuite()
    suite.addTest(TestFun('test_add'))
    runner=unittest.TextTestRunner()
    runner.run(suite)
