"""
功能描述：使用xlutils.copy写excel
编写人：liuyalin
编写时间：2023/1/26 15:12
实现逻辑：
导包
os
xlutils
1. 获取excle路径
2. 打开excel
3. 复制excel
4. 获取复制的excel的sheet页，所有sheet页，某个sheet页
5. 写入
6. 保存
"""
import os
from xlutils.copy import copy
import xlrd
file_path=os.path.dirname(os.path.dirname(__file__))+'//testData//'
file_name='data.xls'
open_excel=xlrd.open_workbook(file_path+file_name)
# 3. 复制excel
wb=copy(open_excel)
# 4. 获取复制的excel的sheet页，所有sheet页，某个sheet页
sheet1=wb.get_sheet(0)
print(sheet1)
# 5. 写入
sheet1.write(9,9,'right')
wb.save(file_path+'data1.xls')
# 6. 保存