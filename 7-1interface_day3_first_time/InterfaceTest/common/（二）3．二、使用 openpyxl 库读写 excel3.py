"""
功能描述：使用openpyxl
编写人：liuyalin
编写时间：2023/1/16 15:00
实现逻辑：
导包openpyxl
1.打开excel
openpyxl.load_workbook
2.获取打开的excel的sheet内容
获取所有的sheet名
获取指定的sheet页
3.获取sheet的最大行数和列数
4.获取某个单元格的值
5.打开将写的表并添加sheet
6.保存
"""
# 1.打开excel
import os

import openpyxl
file_name=os.path.dirname(os.path.dirname(__file__))+r'/testData/data.xlsx'
rb=openpyxl.load_workbook(file_name)
print(rb)
# 2.获取打开的excel的sheet内容
# 获取所有的sheet名
# sheetnames=rb.ger_sheet_names()
# print(sheetnames)
# 获取指定的sheet页
# 3.获取sheet的最大行数和列数
# 4.获取某个单元格的值
# 5.打开将写的表并添加sheet
# 6.保存