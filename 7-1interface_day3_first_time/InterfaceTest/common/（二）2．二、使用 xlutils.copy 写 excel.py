"""
功能描述：使用xlutiles.copy里的copy
编写人：liuyalin
编写时间：2023/1/16 15:00
实现逻辑：
导包xlrd xlutils.copy里的copy
1. 打开excel
2. 复制excel对象
3. 打开复制的sheet页
4. 写入内容
5. 保存excel
"""
# 导包xlrd xlutiles.copy里的copy
import os

import xlrd
from xlutils.copy import copy
# 1. 打开excel
file_path=os.path.dirname(os.path.dirname(__file__))+r'/testData/'
file_name='data.xls'
print(file_path)
rb=xlrd.open_workbook(file_path+file_name)
# 2. 复制excel对象
wb=copy(rb)
# 3. 获取复制的sheet页
ws=wb.get_sheet(0)
# sheet1=rb.sheet_by_index(0)
# 4. 写入内容
ws.write(8,8,9)
wb.save(file_path+'aa'+file_name)
# ws.write(write)
# 5. 保存excel