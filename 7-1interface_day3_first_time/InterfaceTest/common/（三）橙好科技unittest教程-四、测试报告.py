"""
功能描述：
编写人：liuyalin
编写时间：2023/1/25 13:56
实现逻辑：
导包
os
HTMLTestRunner

1. 查找测试用例，生成测试套件
    实例化TestLoader
    调用discover方法
2. 运行测试套件，生成测试报告
    定义报告存放路径，报告名
    以写的模式打开报告---fp
    创建对象runner,实例化HTMLTestRunner(stream=fp,partern='test*.py',top_level_dir=None)
    调用run方法  runner.run(suit)
    关闭文件
3. 清理测试报告
1）全部清理
定位报告目录
获取目录下的所有文件返回列表
遍历列表，删除文件os.remove()
2）如果报告是按时间顺序排列的，按数量清理
定位报告目录
获取目录下的所有文件返回列表
切片，获取列表中从开头到倒数第三个元素组成的列表
遍历列表，删除文件os.remove()
3）如果报告没有按时间顺序排列，按已有报告数量清理
生成报告前：
    获取报告路径
    获取文件名组成列表
    判断文件个数是否超过三个：
        超过，删除：
            获取报告生成时间
            组成报告名和时间对应的字典
            报告时间列表排序
            获取要删除的时间列表delete_list_time=list_time[:-3]
            遍历删除时间列表，获取要删除的文件名列表
            遍历要删除的文件名列表，删除文件
4，发生邮件
"""
# 导包
import os
import time
from HTMLTestRunner import HTMLTestReportCN
import unittest
# 1. 查找测试用例，生成测试套件
#     实例化TestLoader
#     调用discover方法
def creat_suit():
    """
    # 1. 查找测试用例，生成测试套件
#     实例化TestLoader
#     调用discover方法
    :return:
    """
    case_dir=os.path.dirname(os.path.dirname(__file__))+'\\testCase'
    loader=unittest.TestLoader()
    suit=loader.discover(start_dir=case_dir,pattern="test*.py",)
    return suit
report_path=os.path.dirname(os.path.dirname(__file__))+r'/testReport/'
# 2. 运行测试套件，生成测试报告
if __name__=='__main__':
#     定义报告存放路径，报告名

    cur_time=time.strftime("%Y_%m_%d_%H_%M_%S",time.localtime())
    report_name=report_path+cur_time+'report.html'
#     以写的模式打开报告---fp
#     fp=open(report_name,'wb')
    with open(report_name,'wb') as fp:
#   创建对象runner,实例化HTMLTestRunner(stream=fp,partern='test*.py',top_level_dir=None)
        runner=HTMLTestReportCN(stream=fp,title="接口自动化测试报告",description="接口测试结果/用例执行情况")
    #     调用run方法  runner.run(suit)
        suit=creat_suit()
        runner.run(suit)
#     关闭文件
# 3. 清理测试报告
# 1）全部清理
# 定位报告目录
# 获取目录下的所有文件返回列表
# report_list=os.listdir(report_path)
# print(report_list)
# # 遍历列表，删除文件os.remove()
# for i in report_list:
#     os.remove(report_path+'//'+i)
# 2）如果报告是按时间顺序排列的，按数量清理
# 定位报告目录
# 获取目录下的所有文件返回列表
# report_list=os.listdir(report_path)
# print(report_list)
# # 切片，获取列表中从开头到倒数第三个元素组成的列表
# delete_report_list=report_list[:-2]
# # 遍历列表，删除文件os.remove()
# for i in delete_report_list:
#     os.remove(report_path+'//'+i)
# 3）如果报告没有按时间顺序排列，按已有报告数量清理
# 生成报告前：
#     获取报告路径
#     获取文件名组成列表
# report_list=os.listdir(report_path)
# #     判断文件个数是否超过2个：
# if len(report_list)>2:
# #         超过，删除：
# #             获取报告生成时间
#     time_list=[]
#     for i in report_list:
#         file_time=os.path.getctime(report_path+i)
#         time_list.append(file_time)
#     print(time_list)
# #             组成报告名和时间对应的字典
#     name_time_dict={time_list[i]:report_list[i] for i in range(0,len(time_list))}
#     print(name_time_dict)
# #             报告时间列表排序
#     time_list.sort()
#     for i in time_list[:-2]:
#         os.remove(report_path+name_time_dict[i])

# 4，发送邮件
"""
删除报告，先排序，再删除
1.获取文件名列表
2.获取文件生成时间列表
3.组成字典
4.时间列表排序
5.取时间列表范围内，删除文件
"""
# name_list=os.listdir(report_path)
# print(name_list)
# time_list=[]
# for i in name_list:
#     file_time=os.path.getctime(report_path+i)
#     time_list.append(file_time)
# print(time_list)
# dict1={time_list[i]:name_list[i] for i in range(len(time_list))}
# print(dict1)
# for i in time_list[:-2]:
#     os.remove(report_path+dict1[i])