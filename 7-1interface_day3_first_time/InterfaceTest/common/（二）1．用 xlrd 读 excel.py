"""
功能描述：
编写人：liuyalin
编写时间：2023/1/23 15:57
实现逻辑：
导包
1. 
2. 
3. 
"""
# 定位文件位置
import os

import xlrd

file_path=os.path.dirname(os.path.dirname(__file__))+r'/testData'
file_name='/data.xls'
# 打开文件
rd=xlrd.open_workbook(file_path+file_name)
# print(rd)
# 获取指定sheet页
sheet1=rd.sheet_by_name('Sheet1')
# print(sheet1)
# 获取最大行数，列数
nrows=sheet1.nrows
ncols=sheet1.ncols
print(nrows,ncols)
# 获取某个行的值
row_value=sheet1.row_values(1)
print(row_value)
# 获取某个列的值
col_values=sheet1.col_values(1)
print(col_values)
# 获取某个单元格的值
lng=sheet1.cell_value(0,0)
print(lng)