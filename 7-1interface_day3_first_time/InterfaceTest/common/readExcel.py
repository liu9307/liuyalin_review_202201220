"""
功能描述：
编写人：liuyalin
编写时间：2023/1/26 15:00
实现逻辑：
导包
xlrd
1. 获取excel文件路径
2. 打开excel
3. 定位到指定sheet页
4. 获取最大行数/列数
5. 获取某一行的值
6.获取某一列的值
7.获取某个单元格的值
8.某两列的值组成字典
"""
import os
import xlrd
file_path=os.path.dirname(os.path.dirname(__file__))+'//testData//'
file_name='data.xls'
rb=xlrd.open_workbook(file_path+file_name)
sheet1=rb.sheet_by_index(0)
max_row=sheet1.nrows
max_cols=sheet1.ncols
print(max_row,max_cols)
# 5. 获取某一行的值
row_values=sheet1.row_values(0)
print(row_values)
# 6.获取某一列的值
cols_values1=sheet1.col_values(0)
print(cols_values1)
# 7.获取某个单元格的值
value=sheet1.cell_value(0,0)
print(value)
# 8.某两列的值组成字典
cols_values2=sheet1.col_values(1)
print(cols_values2)
dict1={cols_values1[i]:cols_values2[i] for i in range(len(cols_values1))}
print(dict1)