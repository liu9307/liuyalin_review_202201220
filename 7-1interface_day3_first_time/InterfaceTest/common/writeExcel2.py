"""
功能描述：使用openpyxl库读写excel
编写人：liuyalin
编写时间：2023/1/26 15:27
实现逻辑：
导包
os
import openpyxl
1. 定义文件路径，文件名
2. 打开文件
3.定位到文件的指定sheet页
4.获取所有的sheet页名，获取指定的sheet页
5.写入
6.保存
"""
import os
import openpyxl
import xlrd
file_path=os.path.dirname(os.path.dirname(__file__))+'//testData//'
file_name='data.xlsx'
rb=openpyxl.load_workbook(file_path+file_name)
sheet_names=rb.get_sheet_names()
print(sheet_names)