"""
功能描述：
编写人：liuyalin
编写时间：2023/1/26 1:27
实现逻辑：
导包
1. 
2. 
3. 
"""
import unittest
from ddt import ddt,data,unpack,file_data
list1=[1,2,3]
list2=[[1,2],[3,4]]
dict1=[{'value1': 1, 'value2': 2}]
# 1.一次传递一个参数
@ddt
class MyTest1(unittest.TestCase):
    @data(1)
    def test_bb1(self,value):
        print(value)
        self.assertEqual(value,2)
    @data(2,3,4)
    def test_bb2(self,value):
        print(value)
        self.assertEqual(value,2)
    @data(*list1)
    def test_bb3(self,value):
        print(value)
        self.assertEqual(value,1)
# 2.一次传递多个参数
# 1）列表
    @data((1,1))
    @unpack
    def test_bb4(self,value1,value2):
        print(value1,value2)
        self.assertEqual(value1,value2)
    @data([1,2],[3,3])
    @unpack
    def test_bb5(self,value1,value2):
        print(value1,value2)
        self.assertEqual(value1,value2)
    @data(*list2)
    @unpack
    def test_bb6(self,value1,value2):
        print(value1,value2)
        self.assertEqual(value1,value2)
# 2）字典
    @data({'value1':1,'value2':2},{'value1':3,'value2':3})
    @unpack
    def test_bb7(self,value1,value2):
        print(value1,value2)
        self.assertEqual(value1,value2)
    @data(*dict1)
    @unpack
    def test_bb8(self,value1,value2):
        print(value1,value2)
        self.assertEqual(value1,value2)
# 3.json文件
    @file_data('data.json')
    def test_data(self,value):
        print(value)
if __name__=='__main__':
    unittest.main(verbosity=1)
