"""
功能描述：
编写人：liuyalin
编写时间：2022/12/31 12:13
实现逻辑：
导包
1. 
2. 
3. 
"""
# （1）类属性
class Dog(object):
    tooth = 10
    def __init__(self):
        self.age = 5
    def info_print(self):
        print(self.age)
wangcai = Dog()
xiaohei = Dog()
print(Dog.tooth) # 10
print(wangcai.tooth) # 10
print(xiaohei.tooth) # 10

wangcai.tooth=11
print(wangcai.tooth)
# print(Dog.age)   # 类型对象'Dog'没有'age'属性 AttributeError: type object 'Dog' has no attribute 'age'
Dog.tooth=12
print(Dog.tooth)
print(wangcai.tooth)
print(wangcai.age)