"""
功能描述：
编写人：liuyalin
编写时间：2022/12/31 12:48
实现逻辑：
导包
1. 
2. 
3. 
"""
class Dog():
    __tooth=10
    @classmethod
    def get_tooth(cls):
        return cls.__tooth
    @staticmethod
    def info_print():
        print('这是一个狗类，用于创建狗实例')
wangcai=Dog()
print(wangcai.get_tooth())
Dog.info_print()
wangcai.info_print()