"""
功能描述：
编写人：liuyalin
编写时间：2022/12/31 11:49
实现逻辑：
导包
1. 
2. 
3. 
"""
class Dog():
    """
    父类提供统一方法，哪怕是空方法
    """
    def work(self):
        print('指哪打哪')

class ArmyDog(Dog):
    """
    子类重写父类同名方法
    """
    def work(self):
        print('追击敌人')

class DrugDog(Dog):
    def work(self):
        print('追查毒品')

class Person():
    def work_with_dog(self,dog):  # 传入不同对象，执行不同的代码，即不同的work函数
        dog.work()
ad=ArmyDog()
dd=DrugDog()
daqiu=Person()
daqiu.work_with_dog(ad)
daqiu.work_with_dog(dd)
