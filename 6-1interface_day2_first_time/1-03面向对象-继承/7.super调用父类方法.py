"""
功能描述：
编写人：liuyalin
编写时间：2022/12/29 23:50
实现逻辑：
导包
1. 
2. 
3. 
"""
class Master():
    def __init__(self):
        self.kongfu='五香'
    def make_cake(self):
        print(f'{self.kongfu}制作')
class School(Master):
    def __init__(self):
        self.kongfu='香辣'
    def make_cake(self):
        print(f'{self.kongfu}制作')
        # super(School, self).__init__()
        # super(School, self).make_cake()
        super().__init__()
        super().make_cake()
class Prentice(School,Master):
    def __init__(self):
        self.kongfu='独创'
    def make_cake(self):
        self.__init__()
        print(f'{self.kongfu}制作')
    def make_master_cake(self):
        Master.__init__(self)
        Master.make_cake(self)
    def make_school_cake(self):
        School.__init__(self)
        School.make_cake(self)
xiaoming=Prentice()
xiaoming.make_cake()
xiaoming.make_school_cake()
xiaoming.make_master_cake()