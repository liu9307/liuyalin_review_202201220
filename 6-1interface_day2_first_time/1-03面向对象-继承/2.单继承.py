"""
功能描述：
 故事主线：一个煎饼果子老师傅，在煎饼果子界摸爬滚打多年，研发了一套精湛的摊煎饼果子的技术。师父要把这套技术传授给他的唯一的徒弟。
# 分析：徒弟是不是要继承师父的所有技术？
编写人：liuyalin
编写时间：2022/12/29 22:54
实现逻辑：
导包
1. 
2. 
3. 
"""
class Master():
    def __init__(self):
        self.kongfu='五香'
    def make_cake(self):
        print(self.kongfu)
class Prentice(Master):
    pass
xiaoming=Prentice()
xiaoming.make_cake()
# 对象访问实例属性
print(xiaoming.kongfu)