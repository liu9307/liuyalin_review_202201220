"""
功能描述：故事：N年后，xiaoming老了，想要把所有技术传承给自己的徒弟。
编写人：liuyalin
编写时间：2022/12/29 23:37
实现逻辑：
导包
1. 
2. 
3. 
"""
class Master():
    def __init__(self):
        self.kongfu='五香'
    def make_cake(self):
        print(f'{self.kongfu}制作')
class School():
    def __init__(self):
        self.kongfu='香辣'
    def make_cake(self):
        print(f'{self.kongfu}制作')
class Prentice(Master,School):
    def __init__(self):
        self.kongfu='独创'
    def make_cake(self):
        self.__init__()
        print(f'{self.kongfu}制作')
    def make_master_cake(self):
        Master.__init__(self)
        Master.make_cake(self)
    def make_school_cake(self):
        School.__init__(self)
        School.make_cake(self)
class Tusun(Prentice):
    pass

tusun1=Tusun()
print(tusun1.kongfu)
tusun1.make_cake()
tusun1.make_school_cake()
tusun1.make_master_cake()