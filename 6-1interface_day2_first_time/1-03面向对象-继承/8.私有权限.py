"""
功能描述：故事：xiaoming把技术传承给徒弟的同时，不想把自己的钱继承给徒弟，这个时候就要为钱这个实例属性设置私有权限。
编写人：liuyalin
编写时间：2022/12/30 0:00
实现逻辑：
导包
1. 
2. 
3. 
"""
class Master():
    def __init__(self):
        self.kongfu='五香'
        self.__money=500
    def make_cake(self):
        print(f"{self.kongfu}制作")
    def __info_print(self):
        print(self.kongfu)
        print(self.__money)
    def get_money(self):
        print(self.__money)
    def set_money(self):
        self.__money=600
class Prentice(Master):
    pass
xiaoming=Prentice()
# AttributeError: 'Prentice' object has no attribute '__money'
# print(xiaoming.__money)
xiaoming.get_money()
xiaoming.set_money()
xiaoming.get_money()
shifu=Master()
# AttributeError: 'Master' object has no attribute '__money'
# print(shifu.__money)
# AttributeError: 'Master' object has no attribute '__info_print'
# shifu.__info_print()
