"""
功能描述：故事：很多顾客都希望也能吃到古法和橙好的技术的煎饼果子
编写人：liuyalin
编写时间：2022/12/29 23:18
实现逻辑：
导包
1. 
2. 
3. 
"""
class Master():
    def __init__(self):
        self.kongfu='wuxiang'
    def make_cake(self):
        print(f'{self.kongfu}zhizuo')
class School():
    def __init__(self):
        self.kongfu='xiangla'
    def make_cake(self):
        print(f'{self.kongfu}zhizuo')
class Prentice(Master,School):
    def __init__(self):
        self.kongfu='duchuang'
    def make_cake(self):
        self.__init__()
        print(f'{self.kongfu}zhizuo')
    def make_master_cake(self):
        Master.__init__(self)
        Master.make_cake(self)
    def make_school_cake(self):
        # super适用于单继承
        # super().__init__()
        # super().make_cake()
        School.__init__(self)
        School.make_cake(self)
xiaoming=Prentice()
xiaoming.make_master_cake()
xiaoming.make_cake()
xuexiao=School()
print(xuexiao.kongfu)
xiaoming.make_school_cake()