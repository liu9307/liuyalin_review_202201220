"""
功能描述：
编写人：liuyalin
编写时间：2023/1/11 19:38
实现逻辑：
导包
1. 准备接口
2. 准备数据
3. 请求接口并返回结果
4. 查看状态码和结果
5. 获取cookie
6. 获取cookie字典
"""
# 导包
import requests

# 准备接口
url = 'https://wanandroid.com/user/login'
# 准备数据
payload = {'username': '17600409307', 'password': 'LIUyalin123'}
# 请求接口并返回结果
re = requests.post(url=url, data=payload)
# 查看状态码和结果
print(re.status_code)
print(re.text)
# 获取cookie
print('cookie:', re.cookies)
# 获取cookie字典
print('cookie字典:', re.cookies.get_dict())
