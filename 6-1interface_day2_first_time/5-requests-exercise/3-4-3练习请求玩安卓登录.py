"""
功能描述：请求 wanandroid 的登陆接口并判断登陆是否成功（转换结果使用 loads 和 re.json 两种方法实现）
编写人：liuyalin
编写时间：2023/1/10 16:05
实现逻辑：
导包requests
1. 准备接口
2. 准备数据
3. 请求接口并返回结果
4. 打印结果
"""
import json

import requests
url='https://wanandroid.com/user/login'
payload={'username':'17600409307','password':'LIUyalin123'}
re=requests.post(url=url,data=payload)
print(re.status_code)
# print(re.text)
# print(type(re.text))
# 将结果转换为字典格式
# 方法一
print(re.json())
# 方法二
print(json.loads(re.text))
print(type(json.loads(re.text)))
id=re.json()['data']['id']
print(id)
print(re.cookies)