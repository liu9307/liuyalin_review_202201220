"""
功能描述：
接口文档内容：
请求域名：http://httpbin.org
请求方式：post
请求地址：/post
请求参数类型：content-type：json
请求参数：
（随意的参数即可）
username: liangchao
password: 123456
编写人：liuyalin
编写时间：2023/1/10 15:57
实现逻辑：
导包requests
1. 准备接口
2. 准备数据
3. 请求接口并返回结果
4. 打印结果
"""
import requests
import json
url='http://httpbin.org/post'
payload={'username':'liangchao','password':123456}
re=requests.post(url=url,data=json.dumps(payload))
print(re.status_code)
print(re.text)