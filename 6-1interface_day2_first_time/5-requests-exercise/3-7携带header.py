"""
功能描述：
以登陆接口为例：
接口文档内容：
请求域名：https://wanandroid.com
请求方式：post
请求地址：/user/login
请求参数类型：content-type：application/x-www-form-urlencoded(表单)
请求参数：
username: liangchao
password: 123456
编写人：liuyalin
编写时间：2023/1/11 13:08
实现逻辑：
导包
1. 准备接口
2. 准备数据
3. 请求接口并返回结果
4. 打印结果
"""
import requests
url='https://wanandroid.com/user/login'
payload={'username':'17600409307','password':'LIUyalin123'}
header={}
header['User-Agent']='Android'
re=requests.post(url=url,data=payload,headers=header)
print(re.status_code)
print(re.text)