"""
功能描述：
编写人：liuyalin
编写时间：2023/1/11 15:54
实现逻辑：
导包
创建对象
设置cookie
个人信息接口
请求时携带cookie
查看状态码和结果
"""
# 导包
import requests
from requests.cookies import RequestsCookieJar

# 创建对象
jar = RequestsCookieJar()
# 设置cookie，此处的JSESSIONID为提前获取的或者抓包获取的cookie中的关键字段
jar.set('JSESSIONID', '1EDFBF18B3863348E8AA603399AD1506', domain='.wanandroid.com')
print('jar:', jar)
# 准备个人信息接口
url = 'https://wanandroid.com//user/lg/userinfo/json'
# 请求时携带cookie
re = requests.get(url=url, cookies=jar)
# 查看状态码和结果
print(re.status_code)
print(re.text)
