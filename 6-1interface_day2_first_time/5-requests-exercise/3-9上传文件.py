"""
功能描述：
接口文档内容：
请求域名：https://httpbin.org
请求方式：post
请求地址：/post
请求参数：
		文件：test.txt
编写人：liuyalin
编写时间：2023/1/11 14:56
实现逻辑：
导包
1. 
2. 
3. 
"""
import requests
url='https://httpbin.org/post'
file={'file':open('test.txt','rb')}
re=requests.post(url=url,files=file)
print(re.status_code)
print(re.text)
print(type(re.text))