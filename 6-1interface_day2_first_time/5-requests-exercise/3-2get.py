"""
功能描述：
接口文档内容：
请求域名：https://www.kuaidi100.com
请求方式：get
请求地址：/query
请求参数：
type ：任意快递名称的全拼，比如：shunfeng
postid：对应的快递单号，可以是无效的
编写人：liuyalin
编写时间：2023/1/10 10:20
实现逻辑：
导包
1. 准备接口
2. 准备数据
3. 请求接口并返回结果
4. 打印结果
"""
# 导包
import requests

# 准备接口
url = 'https://www.kuaidi100.com/query'
# 准备数据
payload = {'type': 'shunfeng', 'postid': 'SF1383787671701'}
# 请求接口并返回数据
re = requests.get(url=url, params=payload)
print(re.encoding)
