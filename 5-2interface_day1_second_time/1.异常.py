"""
功能描述：
编写人：liuyalin
编写时间：2023/2/8 18:56
实现逻辑：
导包
1. 
2. 
3. 
"""
try:
    print(a)
    print('a')
except Exception as msg:
    print(msg)
else:
    print('运行成功')
finally:
    print('运行结束')


class ShortInputError(Exception):
    def __init__(self,length,min_len):
        self.length=length
        self.min_len=min_len
    def __str__(self):
        return f'当前输入长度是：{self.length},不能少于{self.min_len}个字符'
def main():
    try:
        con=input('输入字符：')
        if len(con)<3:
            raise ShortInputError(len(con),3)
    except Exception as msg:
        print(msg)
main()