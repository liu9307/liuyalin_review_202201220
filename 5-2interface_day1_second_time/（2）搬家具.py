"""
功能描述：需求：将小于房⼦剩余面积的家具摆放到房⼦中
编写人：liuyalin
编写时间：2023/2/8 23:46s
实现逻辑：
导包
1. 定义房子类
定义初始化方法，初始化属性（剩余面积，家具列表）
定义添加家具的方法（家具名，占地面积）

判断剩余面积-家具面积<0:
    提示放不下
>0:
    家具列表追加家具
    剩余面积-=家具面积
2. 定义家具类
定义初始化方法，初始化属性（家具名，占地面积）
"""


class House():
    def __init__(self, address, area):
        self.address = address
        self.area = area
        self.free_area = area
        self.furniture = []

    def add_furniture(self, item):
        if self.free_area - item.area > 0:
            self.free_area -= item.area
            self.furniture.append(item.name)
        else:
            print('放不下了')

    def __str__(self):
        return f'房子位于{self.address},总面积{self.area}，家具有：{self.furniture}，剩余面积{self.free_area}'


class Furniture():
    def __init__(self, name, area):
        self.name = name
        self.area = area


bed = Furniture('床', 4)
xianghe = House('香河', 100)
xianghe.add_furniture(bed)
print(xianghe)