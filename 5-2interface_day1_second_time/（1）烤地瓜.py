"""
功能描述：
（1）烤地瓜
需求主线：
1. 被烤的时间和对应的地瓜状态：
0-3分钟：生的
3-5分钟：半生不熟
5-8分钟：熟的
超过8分钟：烤糊了
2. 添加的调料：
用户可以按自己的意愿添加调料
编写人：liuyalin
编写时间：2023/2/8 23:26
实现逻辑：
导包
1. 定义类地瓜类
定义初始化方法，设置初始化属性：
status
cook_time=0
condiments=【】
2. 定义方法cook（时间）
时间+=时间
判断 if
0-3分钟：生的
3-5分钟：半生不熟
5-8分钟：熟的
超过8分钟：烤糊了
3. 定义添加佐料的方法（佐料）
佐料添加到佐料列表
4. 定义str魔法方法，返回地瓜状态，时间，佐料
"""


class SweetPotato():
    def __init__(self):
        self.status = '生的'
        self.cook_time = 0
        self.condiments = []
    def cook(self,time):
        self.cook_time+=time
        if 0<self.cook_time<3:
            self.status='生的'
        elif 3<=self.cook_time<5:
            self.status='半生不熟'
        elif 5<=self.cook_time<8:
            self.status='熟的'
        elif self.cook_time>=8:
            self.status='糊的'
    def add_condiments(self,*item):
        self.condiments.append(item)
    def __str__(self):
        return f'地瓜烤了{self.cook_time}分钟，状态是{self.status}，添加的佐料有：{self.condiments}'
digua1=SweetPotato()
list1=['盐','辣酱']
digua1.add_condiments(*list1)
digua1.cook(3)
print(digua1)
