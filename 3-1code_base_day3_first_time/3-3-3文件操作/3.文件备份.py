"""
功能描述：需求：⽤户输⼊当前⽬录下任意文件名，程序完成对该文件的备份功能(备份文件名为xx[备份]后缀，例如：test[备份].txt。
编写人：liuyalin
编写时间：2022/12/25 21:17
实现逻辑：
导包
1. 接收要备份的文件名
2. 判断该文件是否能打开
        if能打开:
            # 规划文件名:
            获取 接收文件名格式符.前面的内容 给变量befor_str
            获取 接收文件名格式符.后面的内容 给变量after_str
            拼接 befor_str+'[备份].'+after_str
            # 备份文件写入数据
            w+模式打开新文件名
            遍历原文件每一行:
            读取一行
            写入新文件
    关闭两个文件
"""
old_name=input('请输入要备份的文件名:')
# 判断文件是否是txt文件

subscript_str=old_name.rfind('.')
after_str=old_name[subscript_str:]
if old_name[subscript_str:]=='.txt':
    befor_str=old_name[:subscript_str]
    new_name=befor_str+'[备份]'+after_str
    f1=open(old_name,'r')
    f2=open(new_name,'w+')
    line_num=len(f1.readlines())
    f1.seek(0)
    for i in range(line_num):
        content=f1.readline()
        f2.write(content)
f1.close()
f2.close()