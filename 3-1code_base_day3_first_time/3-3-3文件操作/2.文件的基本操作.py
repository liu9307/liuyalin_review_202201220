"""
功能描述：
编写人：liuyalin
编写时间：2022/12/25 20:52
实现逻辑：
导包
1. 
2. 
3. 
"""
import os
# 打开
f=open('test1.txt', 'w+', encoding='utf-8')
# 写
f.write('一二三\nabc\ndef')
# 读
f.seek(0)
str1=f.read(2)
print(str1)
f.seek(0)
print(f.readline())
print(f.readlines())
# f.seek(2,0)
print(f.tell())
print(f.read())
# f.close()
f1=open('test1.txt', 'r+', encoding='utf-8')
# f1.seek(2,0)
f1.write('333')
f1.close()