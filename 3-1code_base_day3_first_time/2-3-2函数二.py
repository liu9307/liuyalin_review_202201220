"""
功能描述：
编写人：liuyalin
编写时间：2022/12/25 19:16
实现逻辑：
导包
1. 
2. 
3. 
"""
# 变量作用域
# 多函数程序执行流程,共用全局变量
a = 1


def test1():
    global a
    a = 100
    print(a)


# print(a)
def test2():
    print(a)


test1()
test2()
# 返回值作为参数传递
def test3(a):
    return a
def test4(n):
    return n


print(test4(test3(1)))
# 参数
def testA(name,age):
    return f"姓名:{name},年龄{age}"

# 位置参数
print(testA('小明',11))
# 关键字参数
print(testA(name='xiaoming',age=12))
# 缺省参数
def testB(name,age,gender='男'):
    return f"姓名:{name},年龄{age},性别{gender}"
print(testB('小明', age=13))
print(testB('小明',gender='女',age=13))
# 不定长参数/可变参数
# 包裹位置传递
def test1(*args):
    return args
list1=[1,'a',3]
test1(*list1)
test1(1,2,3)
test1(3)
# 包裹关键字
def test2(**kwargs):
    return kwargs
test2(c1=1,c2=2,c3=3)
dict1={'name':'Tom','age':11}
test2(**dict1)
#拆包
a,b=test1(1,2)
print(a,b)
key1,key2,key3=test2(c1=1,c2=2,c3=3)
print(key1,key2,key3)
key1=test2(c1=1).keys()
# dict_keys(['c1'])
print(key1)
# c1 c2
key1,key2=test2(c1=1,c2=2)
print(key1,key2)
value=test2(c1=1).values()
# dict_values([1])
print(value)
dict1={'name':'Tom','age':11}
key1,k2=dict1
# name age
print(key1,k2)
keys=dict1.keys()
# dict_keys(['name', 'age'])
print(keys)
# 交换变量值
a,b=10,20
a,b=b,a
print(a,b)
# 引用
a='a'
b=a
print(id(a))
print(id(b))
a='aa'
print(id(a),id(b))
a=[1,2]
b=a
a.append(3)
print(b,id(a),id(b))
# 引用当实参
def t1(a):
    print(a)
    print(id(a))
    a+=a
    print(a)
    print(id(a))
t1(1)
t1([1,2])