"""
功能描述：5.开发一个注册系统
编写人：liuyalin
编写时间：2022/12/27 0:06
页面：
[{name:xxx,age:xxx},{name:xxx,age:xxx},{name:xxx,age:xxx}]
----------------
*   1-新增用户
*   2-查询用户
*   3-删除用户
----------------
功能1：新增学生信息（姓名和年龄）通过键盘，如果学生已经存在提示用户已存在
功能2：查询学生信息
功能3：删除学生信息
实现逻辑：
导包
1. 定义学生列表
2. 主页展示功能
3. 新增功能
4.查询功能
5.删除功能
"""
list1=[{'name':1,'age':1,'id':1},{'name':2,'age':2,'id':2},{'name':3,'age':3,'id':3}]
# 主页
def homePage():
    """
    主页展示功能
    :return:
    """
    print('-'*8)
    print('*   1-新增用户')
    print('*   2-查询用户')
    print('*   3-删除用户')
    print('-'*8)
# 新增
def addInfo():
    """
    接收id
    遍历列表：
    判断 存在：
        提示存在
    else：
        接收姓名，年龄
        列表追加{‘name':name,'age':age,'id':id}
        提示添加成功，学生信息：{‘name':name,'age':age,'id':id}

    :return:
    """
    id=int(input('id:'))
    for i in list1:
        if i['id']==id:
            print('id已存在')
            print(i)
            break
    else:
        name=input('姓名：')
        age=int(input('年龄：'))
        list1.append({'name':name,'age':age,'id':id})
        print('添加成功')
# addInfo()
# 删除
def removeInfo():
    """
    接收id
    遍历列表
        判断 存在：
            print(i)
            list1.remove(i)
            提示删除成功
            break
    else:
        提示不存在
    :return:
    """
    id=int(input('id:'))
    for i in list1:
        if i['id']==id:
            print(i)
            list1.remove(i)
            print('删除成功')
            break
    else:
        print('id不存在')
# removeInfo()
# 查询
def queryInfo():
    """
    接收姓名
    遍历列表 ：
        判断 存在：
            print i

    :return:
    """
    name=input('姓名：')
    for i in list1:
        if i['name']==name:
            print(i)
while True:
    homePage()
    num=input('选择：')
    if num=='1':
        addInfo()
    elif num=='2':
        queryInfo()
    elif num=='3':
        removeInfo()
    else:
        print('输入有误')