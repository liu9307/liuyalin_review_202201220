"""
功能描述：3.用递归实现n的阶乘:1×2×3×4×.....×n=?
编写人：liuyalin
编写时间：2022/12/26 23:59
实现逻辑：
导包
1. 
2. 
3. 
"""
def multiplication_nums(num):
    if num<=1:
        return 1
    else:
        return multiplication_nums(num-1)*num
print(multiplication_nums(4))