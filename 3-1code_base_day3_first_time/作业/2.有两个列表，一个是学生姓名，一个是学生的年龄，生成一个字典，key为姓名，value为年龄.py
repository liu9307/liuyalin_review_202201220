"""
功能描述：2.有两个列表，一个是学生姓名，一个是学生的年龄，生成一个字典，key为姓名，value为年龄
编写人：liuyalin
编写时间：2022/12/26 23:57
实现逻辑：
导包
1. 
2. 
3. 
"""
name = ['Tom', 'Rose', 'Lily']
age = [16, 17, 18]
info={name[i]:age[i] for i in range(len(name))}
print(info)