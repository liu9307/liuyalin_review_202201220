"""
功能描述：
编写人：liuyalin
编写时间：2022/12/26 23:07
实现逻辑：
导包
1. 
2. 
3. 
"""
f1=lambda:100
print(f1())
f1=lambda a:a
print(f1(1))
f1=lambda a=1:a
print(f1(2))
f1=lambda *args:args
print(f1(1,2))
print(f1([1,2]))
print(f1(*[1,2]))
print(f1((1,2)))
f1=lambda **kwargs:kwargs
print(f1(name='Tom',age=11,tel=12))
dict1={'name':'xiaoming','age':1}
print(f1(**dict1))
