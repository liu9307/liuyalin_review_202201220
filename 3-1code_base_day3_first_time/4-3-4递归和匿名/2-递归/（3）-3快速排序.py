"""
功能描述：快速排序
编写人：liuyalin
编写时间：2022/12/26 14:16
实现逻辑：
导包
1. 定义函数(列表data）：
    判断列表元素个数
        大于等于2时进行分割
            定义左右两个空列表
            设置基准值mid（列表中间的那个元素）
            将mid移除列表
            i遍历列表中的元素：
                跟mid比较
                if i>mid:
                    追加到左列标
                小于等于mid的，追加到右列表
            遍历结束后，拼接列表
            return =函数（左列表）+[mid]+函数（右列表）
        列表元素个数小于2时：
            返回data



2. 
3. 
"""
import random

list1 = [1, 3, 4, 1]


def quit_sort(data):
    if len(data) >= 2:
        left, right = [], []
        mid = data[len(data) // 2]
        data.remove(mid)
        for num in data:
            if num >= mid:
                right.append(num)
            else:
                left.append(num)
        return quit_sort(left) + [mid] + quit_sort(right)
    else:
        return data

list1=[random.randint(0,100) for i in range(1,10)]

print(quit_sort(list1))
