"""
功能描述：
编写人：liuyalin
编写时间：2022/12/26 21:11
实现逻辑：
导包
1. 定义函数move(n,a,b,c):
2. 出口：
    if n==1:
        print(a,'-->',c)
3. 入口：
    else：
        move(n-1,a,c,b)
        move(1,a,b,c)
        move(n-1,b,a,c)
"""
import os


def move(n,a,b,c):
    if n==1:
        print(a,'-->',c)
    else:
        move(n-1,a,c,b)
        move(1,a,b,c)
        move(n-1,b,a,c)
move(3,'A','B','C')
def printAllFiles(path):
    for i in os.scandir(path):
        if i.is_file()==True:
            print(i.path)
        else:
            printAllFiles(i.path)
printAllFiles(r"C:/TEST")