"""
功能描述：递归获取目录下的所有文件
编写人：liuyalin
编写时间：2022/12/26 13:35
实现逻辑：
导包
1.定义函数（目录）：
    遍历目录下的所有文件和文件夹：
        判断  是文件：
                打印绝对路径，带文件名
              是目录：
                调用函数（这个文件i的路径）


"""
import os

file_path = r"C:\TEST"
def printAllFiles(file_path):
    for item in os.scandir(file_path):
        if item.is_file():
            print(f"path:{item.path}")
        elif item.is_dir():
            printAllFiles(item.path)

printAllFiles(file_path)
