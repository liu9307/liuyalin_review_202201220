"""
功能描述：
编写人：liuyalin
编写时间：2022/12/26 20:47
实现逻辑：
导包
1. 定义函数quick_sort(列表）
2. 设置出口：判断列表长度len <2:
                return 列表
3. 入口：
    列表长度len>=2:
        定义左右空列表
        定义分界值mid=列表[len(列表）//2]
        for遍历列表中的数据：
            判断 i>mid:
                添加到右列表
            else:
                添加到左列表
        return 函数（左列表）+[mid]+函数（右列表）
    else：
        return 列表
"""
import random


def quick_sort(data):
    if len(data) < 2:
        return data
    else:
        left, right = [], []
        mid = data[len(data) // 2]
        data.remove(mid)
        for i in data:
            if i < mid:
                left.append(i)
            else:
                right.append(i)
        return quick_sort(left) + [mid] + quick_sort(right)


if __name__ == '__main__':
    print(quick_sort([random.randint(1, 100) for i in range(10)]))
