"""
功能描述：a．存在以下目录结构，通过os.scandir遍历目录
编写人：liuyalin
编写时间：2022/12/26 12:42
实现逻辑：
导包
1. 
2. 
3. 
"""
import os
for i in os.scandir("C:\TEST"):
    print(f"name:{i.name}")
    print(f"绝对路径:{i.path}")
    print(f"是否是文件：{i.is_file()}")
    print(f"是否是文件夹：{i.is_dir()}")