"""
功能描述：
编写人：liuyalin
编写时间：2022/12/26 23:29
实现逻辑：
导包
1. 
2. 
3. 
"""
print(abs(-1))
print(round(3.44))
# 需求：任意两个数字，按照指定要求整理数字后再进⾏求和计算。
def sum(func,a,b):
    return func(a)+func(b)
print(sum(abs,-1,-2))
# map()
list1=[-1,-2,1,2,3]
print(map(abs,list1))
print(list(map(abs,list1)))
for i in map(abs,list1):
    print(i)
# reduce()
import functools
def sum(a,b):
    return a+b
functools.reduce(sum,list1)
print(functools.reduce(sum,list1))
# filter()
f1=lambda x:x%2==2
print(filter(f1,list1))
list1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]


def func(x):
    return x % 2 == 0


result = filter(func, list1)
# <filter object at 0x000002D51EF87DD8>
print(result)
for i in filter(func, list1):
    print(i)
# <filter object at 0x00000178A7C87DD8>
# 2
# 4
# 6
# 8
# 10
