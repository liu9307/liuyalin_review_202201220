"""
功能描述：
需求：进⼊系统显示系统功能界⾯，功能如下：
1、添加学员
2、删除学员
3、修改学员信息
4、查询学员信息
5、显示所有学员信息
6、退出系统
系统共6个功能，⽤户根据⾃⼰需求选取。
编写人：liuyalin
编写时间：2022/12/25 23:17
实现逻辑：
导包
1. 主页面展示
******欢迎使用学员管理系统*****
1、添加学员
2、删除学员
3、修改学员信息
4、查询学员信息
5、显示所有学员信息
6、退出系统
**********************
2. 添加学员功能页面
    接收学员学员id
    遍历学员列表
    判断id是否重复
        不重复:接收姓名,年龄
                接收是否确认添加
                判断,y确认:追加到列表
                    else:
                        提示取消添加操作,系统将返回主页面
                            显示主页面
        重复:提示id重复,系统将返回主页面
             显示主页面
3. 删除学员功能页
    接收学员学员id
    遍历学员列表
        判断id是否存在:
            存在:移除
                返回删除的学员信息
                提示删除成功,系统将返回主页面
                显示主页面
            不存在:提示该id不存在,系统将返回主页面
                显示主页面
4.修改功能页
    接收学员学员id
    遍历学员列表:
        判断id是否存在:
            存在:接收学员修改的信息
                接收是否确认修改
                判断,y确认:修改
                    else:
                        提示取消修改操作,系统将返回主页面
                            显示主页面
            不存在:提示该id不存在,系统将返回主页面
                显示主页面
5.查询功能页
    接收学员学员姓名
    遍历学员列表:
        判断姓名是否存在:
            存在:接收姓名的学员信息,系统将返回主页面
                            显示主页面
            不存在:提示不存在,系统将返回主页面
                显示主页面
6.显示所有学员信息
遍历学员列表:
    显示学员信息
7.退出系统
接收确认退出:
y:提示退出成功,感谢使用学员管理系统!
else:取消退出,系统将返回主页面
     显示主页面
"""
stu_list=[{'name':'name1','age':1,'stu_id':1}]
def homePage():
    """
    # 主页面展示
    :return:
    """
    print('欢迎使用学员管理系统'.center(40,'*'))
    print('1、添加学员\n2、删除学员\n3、修改学员信息\n4、查询学员信息\n5、显示所有学员信息\n6、退出系统')
    print('*'*50)
# homePage()
# 2. 添加学员功能页面
def addInfo():
    """
    添加学员功能
    :return:
    """
# 接收学员学员id
    new_id=int(input('学员id:'))
# 遍历学员列表
    for i in stu_list:
        # 判断id是否重复
        if i['stu_id']==new_id:
            # 重复:提示id重复,系统将返回主页面
            print('学员id重复\n系统将返回主页面')
            break
    else:
        # 不重复:接收姓名,年龄
        new_name=input('姓名:')
        new_age=int(input('年龄:'))
        # 接收是否确认添加
        confirm=input("是否确认提交添加,y添加,n取消:")
        # 判断,y确认:追加到列表
        if confirm=='y':
            new_info={'name':new_name,'age':new_age,'stu_id':new_id}
            stu_list.append(new_info)
            print(f"添加成功!\n新学员信息:姓名:{new_name},年龄:{new_age},学员id:{new_id}\n系统将返回主页面")
        #else:提示取消添加操作,系统将返回主页面
        else:
            print(f"取消添加操作\n系统将返回主页面")

# 3. 删除学员功能页
def removeInfo():
    """删除学员功能"""
    #     接收学员学员id
    remove_id=int(input('输入要删除的学员id:'))
    #     遍历学员列表
    for i in stu_list:
        #         判断id是否存在:
        #             存在:移除
        if i['stu_id']==remove_id:
            #  返回要删除的学员信息
            print(f"学员信息:姓名:{i['name']},年龄:{i['age']},学员id:{i['stu_id']}\n")
            # 接收是否确认添加
            confirm=input("是否确认删除,y删除,n取消:")
            # 判断,y确认:
            if confirm=='y':
                stu_list.remove(i)

    #                 提示删除成功,系统将返回主页面
                print('删除成功\n系统将返回主页面')
                break
            else:
                print('取消删除操作\n系统将返回主页面')
                break
#             不存在:提示该id不存在,系统将返回主页面
    else:
        print('该学员id不存在\n系统将返回主页面')
# remove_info()
# 4.修改功能页
def editInfo():
#     接收学员学员id
    id=int(input('输入原学员id:'))
#     遍历学员列表:
    for i in stu_list:
#         判断id是否存在:
        if i['stu_id']==id:
#             存在:接收学员修改的信息
            edit_id=int(input('学员id修改为:'))
            edit_name=input('姓名修改为:')
            edit_age=int(input('年龄修改为:'))
            confirm=input('是否确认修改,y修改,n取消:')
#                 接收是否确认修改
#                 判断,y确认:修改
            if confirm=='y':
                i['name']=edit_name
                i['age']=edit_age
                i['stu_id']=edit_id
#                     else:
#                         提示取消修改操作,系统将返回主页面
            else:
                print("消修改操作\n系统将返回主页面")
                break
#             不存在:提示该id不存在,系统将返回主页面
    else:
        print('该学员id不存在\n系统将返回主页面')
# 5.查询功能页
def searchInfo():
#     接收学员学员姓名
    name=input('输入查找姓名:')
#     遍历学员列表:
    for i in stu_list:
        if i['name']==name:
#         判断姓名是否存在:
#             存在:展示接收姓名的学员信息
            print(f"学员信息:姓名:{i['name']},年龄:{i['age']},学员id:{i['stu_id']}\n")
#             不存在:提示不存在,系统将返回主页面
    else:
        print('该姓名不存在\n系统将返回主页面')
# searchInfo()
# 6.显示所有学员信息
def allInfo():
    print("姓名/年龄/学员id")
    # 遍历学员列表:
    #     显示学员信息
    for i in stu_list:
        print(f"{i['name']}/{i['age']}/{i['stu_id']}")


# allInfo()

while True:
    homePage()
    select_num=input('输入功能对应的序号:')
    # 1、添加学员
    # 2、删除学员
    # 3、修改学员信息
    # 4、查询学员信息
    # 5、显示所有学员信息
    # 6、退出系统
    if select_num=='1':
        addInfo()
    elif select_num=='2':
        removeInfo()
    elif select_num=='3':
        editInfo()
    elif select_num=='4':
        searchInfo()
    elif select_num=='5':
        allInfo()
    elif select_num=='6':
        confirm = input('是否确认退出,y退出,n取消:')
        if confirm == 'y':
            print('感谢使用学员管理系统!\n退出成功!')
            break
        else:
            print('取消退出操作\n系统将返回主页面')
    else:
        print('输入有误')
