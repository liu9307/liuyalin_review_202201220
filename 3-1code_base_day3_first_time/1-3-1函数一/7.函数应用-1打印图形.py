"""
功能描述：
编写人：liuyalin
编写时间：2022/12/25 15:22
实现逻辑：
导包
1. 
2. 
3. 
"""


# 一条横线
def printLine():
    print('-' * 20)


# 打印多条横线
def printLines(num):
    """
    打印多条横线
    :param num: 想要打印的横线数量
    :return:
    """
    for i in range(num):
        printLine()


if __name__ == '__main__':
    printLines(5)
