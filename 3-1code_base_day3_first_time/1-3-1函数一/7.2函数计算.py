"""
功能描述：
编写人：liuyalin
编写时间：2022/12/25 15:30
实现逻辑：
导包
1. 
2. 
3. 
"""
# 求三个数之和
def sumNums(a, b, c):
    return a + b + c


result = sumNums(1, 2, 3)
print(result)


# 求三个数的平均数
def averageNums(a, b, c):
    sum_result = sumNums(a, b, c)
    return sum_result / 3


result = averageNums(1, 2, 3)
print(result)
