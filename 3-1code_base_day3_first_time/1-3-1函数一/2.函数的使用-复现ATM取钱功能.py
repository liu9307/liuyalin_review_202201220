"""
功能描述：复现ATM取钱功能
编写人：liuyalin
编写时间：2022/12/25 14:04
实现逻辑：
导包
1. 搭建整体框架（复现需求）
    登录页
        输入卡号、密码
        确认登录
    主页展示
        查询
        转账
        取款
        退出
    功能页
        查询功能页
            余额展示
            返回主页按钮
        转账功能页
            输入卡号
            输入密码
            确认转账
            提示转账成功/失败、返回主页面
        取款功能页
            输入取款金额
            输入密码
            确认取款
            提示取款成功/失败、返回主页面
        退出功能页
            确认退出
            提示退出成功/取消退出，返回主页面
2.封装代码
3. 调用函数
"""
#     登录页
#         输入卡号、密码
#         确认登录
def loginPage():
    card_num=int(input('输入卡号：'))
    password=input('输入密码：')
    confirm_login=input('确认登录？y登录,n取消:')
    if confirm_login=='y':
        # 主页展示
        homePage()
        pass
# 主页展示
#     查询
#     转账
#     取款
#     退出
def homePage():
    print('*'*20)
    print("1-查询\n2-转账\n3-取款\n4-退出")
    print('*'*20)
    choose_num=int(input("请输入功能序号："))
    if choose_num==1:
        inquiryFunc()
        homePage()
    elif choose_num==2:
        # 调用转账函数
        homePage()
    # elif ....
    else:
        print('输入错误')
        homePage()

# 功能页
#     查询功能页
#         余额展示
def inquiryFunc():
    print("余额：n元人民币")


if __name__ == '__main__':
    loginPage()
