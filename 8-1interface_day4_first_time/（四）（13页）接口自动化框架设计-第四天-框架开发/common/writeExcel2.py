"""
功能描述：提供testcase模块写入excel的方法
导包
os
xlrd
xlutils.copy
创建一个类
1.定义一个__init__初始化方法
1.1 获取文件路径（相对路径）
1.2 打开文件
1.3 复制
1.4 确定sheet页
2.定义一个对外方法：writeData
2.1 写入的行和列 需要外界给
2.2 写入的数据 需要外界给
2.3 调用write方法写入
2.4 保存
"""
import os
import xlrd
from xlutils.copy import copy


class WriteExcel():
    def __init__(self):
        """
        初始化excel的属性
        """
        # 准备文件路径和文件名
        self.excel_dir = os.path.dirname(os.path.dirname(__file__)) + '//testData//'
        self.excel_name = 'data.xls'
        # 打开文件
        self.wb = xlrd.open_workbook(self.excel_dir + self.excel_name)
        # 复制
        self.cb = copy(self.wb)
        # 获取指定sheet页
        self.cs = self.cb.get_sheet(0)

    def writeData(self, id, real, status):
        """
        对外提供写入excel的犯法
        :param id: 代表测试数据的行
        :param real: 代表接口测试的实际结果
        :param status: 代表用例的执行状态
        :return:
        """
        self.cs.write(id, 6, real)
        self.cs.write(id, 7, status)
        self.cb.save(self.excel_dir + 'data1.xls')


if __name__ == '__main__':
    we = WriteExcel()
    we.writeData(1, 'real', 'status')
