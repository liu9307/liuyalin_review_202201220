"""
功能描述：提供testCase模块写入excel的方法
编写人：liuyalin
编写时间：2023/1/31 22:48
实现逻辑：
导包
os
xlrd
xlutils.copy
1. 定义类WriteExcel
2. 定义初始化方法
获取文件路径
打开文件
复制文件
获取sheet
3. 定义写入方法（需要外界提供参数）
写入excel
保存
"""
import os
import xlrd
from xlutils.copy import copy
from log import logger
class WriteExcel():
    def __init__(self):
        self.file_path=os.path.dirname(os.path.dirname(__file__))+'//testData//data.xls'
        self.wb=xlrd.open_workbook(self.file_path)
        self.cb=copy(self.wb)
        self.cs=self.cb.get_sheet(0)
    def writeData(self,id,real,status):
        self.cs.write(id,6,real)
        self.cs.write(id,7,status)
        self.cb.save(self.file_path)
if __name__ == '__main__':
    we=WriteExcel()
    we.writeData(6,'real','status')