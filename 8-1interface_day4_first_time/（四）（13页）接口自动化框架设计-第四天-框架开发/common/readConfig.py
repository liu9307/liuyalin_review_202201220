"""
功能描述：
编写人：liuyalin
编写时间：2023/2/1 14:48
实现逻辑：
导包
os
configParser
log
1. 定义类ReadConfig
2. 定义初始化方法
设置初始化属性：
创建ConfigParser对象
获取config.ini文件路径
准备文件名=文件名
调用read方法读取文件
3. 定义读取方法readConfig（传参section,option='all')
if option=='all':
    如果没给option，获取section下的所有键值对
    返回结果
else:如果给option，获取某个option的值
    返回结果
"""
# 导包
# os
# configParser
# log
import os
from log import logger
from configparser import ConfigParser


# 1. 定义类ReadConfig
class ReadConfig():
    # 2. 定义初始化方法
    def __init__(self):
        # 设置初始化属性：
        # 创建ConfigParser对象
        # 获取config.ini文件路径
        # 准备文件名=文件名
        # 调用read方法读取文件
        self.conf = ConfigParser()
        file_path = os.path.dirname(os.path.dirname(__file__))
        file_name = '//config.ini'
        self.conf.read(file_path + file_name,encoding='utf-8-sig')

    # 3. 定义读取方法readConfig（传参section,option='all')
    # if option=='all':
    #     如果没给option，获取section下的所有键值对
    #     返回结果
    # else:如果给option，获取某个option的值
    #     返回结果
    def get_config(self, section, option='all'):
        if option == 'all':
            logger.debug(f'items:{self.conf.items(section)}')
            return self.conf.items(section)
        else:
            logger.debug(f'option:{option}\t值:{self.conf.get(section, option)}')
            return self.conf.get(section, option)


if __name__ == '__main__':
    rc = ReadConfig()
    rc.get_config('mysql')
    rc.get_config('mysql', 'host')
