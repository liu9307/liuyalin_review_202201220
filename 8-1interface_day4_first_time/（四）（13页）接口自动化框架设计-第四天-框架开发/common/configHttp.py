"""
功能描述：
编写人：liuyalin
编写时间：2023/1/31 15:01
实现逻辑：
导包
requests
1. 接收testcase传入的请求参数
    id,interfaceUrl,name,method,value,expect,real,status
2. 根据接口的请求方式进行判断
    get--requests.get
    post--requests.post
3. 返回接口执行后的结果
"""
import requests
from log import logger


class ConfigHttp():
    def __init__(self, interfaceUrl, method, value):
        # 接受testcase传入的请求参数
        # id,interfaceUrl,name,method,value,expect,real,status
        self.method = method
        self.interfaceUrl = interfaceUrl
        self.value = eval(value)
        # self.header={}

    def __get(self):
        try:
            re = requests.get(url=self.interfaceUrl, params=eval(self.value))
        except Exception as msg:
            logger.error(f'接口请求失败，系统提示：{msg}')
        else:
            return re

    def __post(self):
        try:
            logger.debug(f"url:{self.interfaceUrl},data:{self.value}")
            re = requests.post(url=self.interfaceUrl, data=self.value)
        except Exception as msg:
            logger.error(f'接口请求失败，系统提示：{msg}')
        else:
            return re

    def __put(self):
        try:
            re = requests.put(url=self.interfaceUrl, data=eval(self.value))
        except Exception as msg:
            logger.error(f'接口请求失败，系统提示：{msg}')
        else:
            return re

    def __delete(self):
        try:
            re = requests.delete(url=self.interfaceUrl)
        except Exception as msg:
            logger.error(f'接口请求失败，系统提示：{msg}')
        else:
            return re

    # 根据接口的请求方式进行判断
    def run(self):
        logger.debug(f'method：{self.method}')
        if self.method.lower() == 'get':
            re = self.__get()
        elif self.method.lower() == 'post':
            re = self.__post()
            logger.debug(f'status:{re.status_code}')
        elif self.method.lower() == 'put':
            re = self.__put()
        elif self.method.lower() == 'delete':
            re = self.__delete()
        else:
            print('未找到匹配的请求方式，请检查！')
if __name__ == '__main__':
    url='https://wanandroid.com/user/login'
    method='post'
    value="{'username':'liangchao','password':'123456'}"
    ch=ConfigHttp(url,method,value)
    print(ch.run())