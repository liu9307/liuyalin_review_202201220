"""
功能描述：
编写人：liuyalin
编写时间：2023/1/31 1:43
实现逻辑：
导包
os
xlrd
1.创建__init__初始化方法
1.1获取excel文件路径
1.2打开excel文件
1.3获取指定的sheet页
1.4获取最大行数、列数
1.5预设一个返回列表，初始为空
1.6获取第一行的值作为结果的key
2.定义一个组装数据的对外方法：read
2.1循环获取每一行数据（第一行除外）
2.2与第一行数据组装为一个字典
2.3将组装好的字典添加到结果列表
2.4返回结果列表
"""
import os
import xlrd
from log import logger

class ReadExcel():
    # 1.创建__init__初始化方法
    def __init__(self):
        # 1.1获取excel文件路径
        self.path_name = os.path.dirname(os.path.dirname(__file__)) + '//testData//data.xls'
        # 1.2打开excel文件
        self.readbook = xlrd.open_workbook(self.path_name)
        # 1.3获取指定的sheet页
        self.sheet = self.readbook.sheet_by_index(0)
        # 1.4获取最大行数、列数
        self.max_row = self.sheet.nrows
        self.max_col = self.sheet.ncols
        # 1.5预设一个返回列表，初始为空
        self.res_list = []
        # 1.6获取第一行的值作为结果的key
        self.first_row = self.sheet.row_values(0)

    # 2.定义一个组装数据的对外方法：read
    def read(self):
        # 2.1循环获取每一行数据（第一行除外）
        for i in range(1, self.max_row):
            # 2.2与第一行数据组装为一个字典
            i_row_values = self.sheet.row_values(i)
            dict1 = {self.first_row[j]: i_row_values[j] for j in range(len(self.first_row))}
            # 2.3将组装好的字典添加到结果列表
            self.res_list.append(dict1)
        # 2.4返回结果列表
        logger.debug(f'打印列表：{self.res_list}')
        return self.res_list


if __name__ == '__main__':
    re = ReadExcel()
    re.read()

