"""
功能描述：
编写人：liuyalin
编写时间：2023/1/31 20:37
实现逻辑：
导包
requests
1. 定义类
2. 定义初始化方法，接收testcase传入的参数请求
    初始化参数interfaceUrl,method,value
3. 定义请求方式私有方法，返回请求对象
4. 根据接口的类型判断请求方式
"""
from log import logger
import requests
class ConfigHttp():
    def __init__(self,interfaceUrl,method,value):
        self.url=interfaceUrl
        self.method=method
        self.value=eval(value)
    def __get(self):
        try:
            re=requests.get(url=self.url,params=self.value)
        except Exception as msg:
            logger.error(f'接口请求失败，系统提示：{msg}')
        else:
            return re
    def __post(self):
        try:
            re=requests.post(url=self.url,data=self.value)
        except Exception as msg:
            logger.error(f'接口请求失败，系统提示：{msg}')
        else:
            return re
    def __delete(self):
        try:
            re=requests.delete(url=self.url)
        except Exception as msg:
            logger.error(f'接口请求失败，系统提示：{msg}')
        else:
            return re
    def __put(self):
        try:
            re=requests.put(url=self.url,data=self.value)
        except Exception as msg:
            logger.error(f'接口请求失败，系统提示{msg}')
        else:
            return re
    def run(self):
        if self.method=='get':
            re=self.__get()
        elif self.method=='post':
            re=self.__post()
            logger.debug(f'状态码:{re.status_code}')
        elif self.method=='put':
            re=self.__put()
        elif self.method=='delete':
            re=self.__delete()
        else:
            print('未找到匹配的请求方式，请检查！')
if __name__=='__main__':
    url='https://www.wanandroid.com/user/login'
    method='post'
    value="{'username':'liangchao','password':'123456'}"
    ch=ConfigHttp(url,method,value)
    re=ch.run()
