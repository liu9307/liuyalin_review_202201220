# """
# 功能描述：定义一个外界的函数log
# 编写人：liuyalin
# 编写时间：2023/1/31 2:37
# 实现逻辑：
# 导包
# os
# logging
# logging.handlers
# 1. 设置日志记录器名称
# 2. 设置日志级别
# 3. 设置日志输出格式
# 4. 创建并添加handler，控制台输出
#     4.1实例化logging.StreamHandler，创建对象sh
#     4.2设置日志输出格式
#     4.3logger对象添加sh
# 5. 创建并添加handler，文件输出
#     5.1实例化logging.TimedRotatingFileHandler，创建对象tfh
#     5.2设置tfh.suffix='时间格式'
#     5.2设置日志输出格式
#     5.3logger对象添加tfh
# """
import os
import logging
import logging.handlers
def log():
# 1. 设置日志记录器名称
    logger=logging.getLogger('Interface')
# 2. 设置日志级别
    logger.setLevel(logging.DEBUG)
# 3. 设置日志输出格式
    format=logging.Formatter('%(name)s-日志级别：%(levelname)s-线程id:%(thread)d-线程名：%(threadName)s-进程：%(process)s-日志生成时间：%(asctime)s-文件名:%(filename)s-模块名：%(module)s-行号：%(lineno)d-内容：%(message)s')
# 5. 创建并添加handler，文件输出
#     5.1实例化logging.TimedRotatingFileHandler，创建对象tfh
    log_path=os.path.dirname(os.path.dirname(__file__))+'//testLog//timeRotatingHandler.log'
    trfh=logging.handlers.TimedRotatingFileHandler(filename=log_path,when='S',interval=1,backupCount=1)
#     5.2设置tfh.suffix='时间格式'
    trfh.suffix="%Y-%m-%d_%H-%M-%S.log"
#     5.2设置日志输出格式
    trfh.setFormatter(format)
#     5.3logger对象添加tfh
    logger.addHandler(trfh)
# 4. 创建并添加handler，控制台输出
#     4.1实例化logging.StreamHandler，创建对象sh
    sh=logging.StreamHandler()
#     4.2设置日志输出格式
    sh.setFormatter(format)
#     4.3logger对象添加sh
    logger.addHandler(sh)

    return logger
logger=log()
if __name__ == '__main__':
    logger.info('这是日志')
