"""
功能描述：
编写人：liuyalin
编写时间：2023/1/30 21:40
实现逻辑：
导包
os,configparser
1. 实例化configparser类，创建conf对象
2. 定义config.ini文件名，含具体路径
3. 读取config.ini文件内容
4. 获取文件中的所有secsions，返回列表
5. 获取section下的所有option，返回列表
6. 获取section下的option键值对，返回字典
7. 获取某个option的值，返回字符串
"""
import os
import configparser
# 1. 实例化configparser类，创建conf对象
conf=configparser.ConfigParser()
# 2. 准备config.ini文件名，含具体路径
conf_name=os.path.dirname(__file__)+'//config.ini'
# 3. 读取config.ini文件内容
conf.read(conf_name,encoding='utf-8-sig')
# 4. 获取文件中的所有secsions，返回列表
sections=conf.sections()
# ['mysql', 'redis']
print(sections)
print(type(sections))
# 5. 获取section下的所有option，返回列表
options=conf.options('mysql')
# ['host', 'port', 'user', 'pwd']
print(options)
print(type(options))
# 6. 获取section下的option键值对，返回元组组成的列表
items=conf.items('mysql')
# [('host', '127.0.0.1'), ('port', '3306'), ('user', 'liu'), ('pwd', '123')]
print(items)
print(type(items))
# 7. 获取某个option的值，返回字符串
value=conf.get('mysql','host')
# 127.0.0.1
print(value)
print(type(value))
