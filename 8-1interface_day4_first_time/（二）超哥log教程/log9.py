"""
功能描述：
编写人：liuyalin
编写时间：2023/1/29 19:57
实现逻辑：
导包os,logging.handlers,logging
1. 设置日志记录器名称
2. 设置日志级别
3. 设置日志格式
4.创建添加handler,输出到文件
5.创建添加handler ，输出到控制台
"""
import os
import logging
import logging.handlers
def log():
    logger=logging.getLogger('Interface')
    logger.setLevel(logging.INFO)
    format=logging.Formatter('%(name)s-文件名：%(filename)s-日志级别：%(levelname)s-日志生成时间：%(asctime)s-模块名：%(module)s-行号：%(lineno)d-内容：%(message)s')
    # 创建添加handler，输出到控制台
    sh=logging.StreamHandler()
    sh.setFormatter(format)
    logger.addHandler(sh)
    # 创建并添加handler,输出到文件
    fh=logging.handlers.RotatingFileHandler(filename='rotatinglog.log',mode='a',maxBytes=100,backupCount=3,encoding='utf-8')
    fh.setFormatter(format)
    logger.addHandler(fh)
    return logger
logger=log()
if __name__ == '__main__':
    logger.info('练习log高级用法logging.handlers.RotatingFileHandler')