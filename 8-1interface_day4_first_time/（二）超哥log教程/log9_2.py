"""
功能描述：
编写人：liuyalin
编写时间：2023/1/29 20:27
实现逻辑：
导包
1. 
2. 
3. 
"""
import os
import logging
import logging.handlers
def log():
    logger=logging.getLogger('Interface')
    logger.setLevel(logging.INFO)
    format=logging.Formatter('%(name)s-%(filename)s-%(module)s-%(levelname)s-%(asctime)s-%(lineno)d-%(message)s')
    fh=logging.handlers.TimedRotatingFileHandler('timedrotatinghandler.log',when='S',interval=2,backupCount=3)
    fh.suffix="%Y-%m-%d_%H-%M-%S.log"
    fh.setFormatter(format)
    logger.addHandler(fh)
    sh=logging.StreamHandler()
    sh.setFormatter(format)
    logger.addHandler(sh)
    return logger
logger=log()
if __name__ == '__main__':
    logger.info('rizhi')