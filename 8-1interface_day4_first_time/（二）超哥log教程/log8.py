"""
步骤:
导包logging,os
1.配置日志记录器名称
2.配置日志级别
3.配置日志格式(可以分别设置,也可以统一设置)
4.创建并添加handler-控制台输出
    创建StreamHandler对象sh
    设置日志格式
    logger对象添加sh
5.创建并添加handler-文件输出
    创建FileHandler对象fh
    设置日志格式
    logger对象添加fh
6.提供对外获取logger
"""
import os, logging


def log():
    # 1.配置日志记录器名称
    logger = logging.getLogger('Interface')
    # 2.配置日志级别
    logger.setLevel(logging.DEBUG)
    # 3.配置日志格式(可以分别设置,也可以统一设置)
    format = logging.Formatter('日志记录器：%(name)s-日志级别：%(levelname)s-日志生成时间：%(asctime)s-文件名：%(filename)s-模块名：%(module)s-行号：%(lineno)d-内容：%(message)s')
    # 4.创建并添加handler-输出到控制台
    #     创建StreamHandler对象sh
    sh = logging.StreamHandler()
    #     设置日志格式
    sh.setFormatter(format)

    # 5.创建并添加handler-输出到文件
    #     创建FileHandler对象fh
    file_name = os.path.dirname(__file__) + '//logfile.txt'
    fh = logging.FileHandler(file_name)
    #     设置日志格式
    fh.setFormatter(format)

    # 6.提供对外获取logger
    print(logger.handlers)
    if not logger.handlers:
        #     logger对象添加sh
        logger.addHandler(sh)
        #     logger对象添加fh
        logger.addHandler(fh)
    return logger


# logger = log()
if __name__ == '__main__':

    logger.info('这是日志')
