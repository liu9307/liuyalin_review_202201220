"""
功能描述：
编写人：liuyalin
编写时间：2023/1/27 21:34
实现逻辑：
导包logging
1. 配置日志
2. 设置日志记录器
3. 
"""
import logging


def log():
    # 使用basicConfig配置日志，level和format
    logging.basicConfig(level=logging.DEBUG,
                        format='%(name)s日志-级别:%(levelname)s-模块:%(module)s.py-第%(lineno)d行:%(message)s')
    # 使用getLogger方法设置日志记录器，不设置的话默认：root
    logger = logging.getLogger('Interface')
    return logger


logger = log()
logger.info('这是定制化的日志')
