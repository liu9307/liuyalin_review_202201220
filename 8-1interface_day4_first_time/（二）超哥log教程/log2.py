"""
功能描述：
编写人：liuyalin
编写时间：2023/1/29 19:03
实现逻辑：
导包
1. 
2. 
3. 
"""
import os,logging
class Log():
    def __init__(self):
        self.logger=logging.getLogger()
        self.level=self.logger.setLevel(logging.INFO)
        self.format=logging.Formatter('%(name)s-%(levelname)s-%(filename)s-%(asctime)s-模块：%(module)s-行号：%(lineno)d-内容：%(message)s')
    def __addStreamHandler(self):
        sh=logging.StreamHandler()
        sh.setFormatter(self.format)
        self.logger.addHandler(sh)
    def __addFileHandler(self):
        file_name=os.path.dirname(__file__)+'//logfileclass.txt'
        fh=logging.FileHandler(file_name,encoding='utf-8')
        fh.setFormatter(self.format)
        self.logger.addHandler(fh)
    def getLog(self):
        self.__addStreamHandler()
        self.__addFileHandler()
        return self.logger
if __name__ == '__main__':
    logger=Log().getLog()
    logger.info('这是日志')