"""
功能描述：实现邮件的配置，和对外提供发送邮件的方法
编写人：liuyalin
编写时间：2023/2/6 15:52
实现逻辑：
导包
os,time,SMTP
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.header import Header
1. 定义类SendEmail
2. 定义初始化方法，设置初始化属性
发件邮箱服务器
发件人邮箱地址
发件人邮箱登录名
发件密码SMTP授权码
定义时间格式t
邮件标题+t
3. 定义私有方法，配置邮件__config
获取附件地址
组装附件名称
打开附件
获取附件内容给变量mail_body
实例化MIMEMultipart，创建邮件对象
实例化MIMEText，创建附件对象
准备附件内容：
附件对象追加附件内容mail_body
附件对象添加Content-Type='application/octet-stream'
附件对象设置附件名称Content-Disposition='attachment;filename=result.html附件名称
邮件对象追加附件
准备邮件文本内容：
content
邮件追加文本内容content（Header(content,'plain','utf-8')
邮件添加Subject,From,To
4.定义提供对外的方法send发送邮件
调用配置邮件方法配置邮件
创建对象s，实例化SMTP类
连接邮箱服务器
登录邮箱
调用sendmail方法发送邮件（传入发件人，收件人，self.msg.as_string())
"""
import os,time,smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.header import Header
class SendEmail():
    def __init__(self):
        self.email_server='smtp.qq.com'
        self.sender='245555126@qq.com'
        self.password='jdnrnylhlaobbjjd'
        self.receiver='17600409307@163.com'
        t=time.strftime('%Y-%m-%d_%H-%M-%S',time.localtime())
        self.subject='自动化测试结果'+t
    def __config(self,file):
        with open(file,'r') as f:
            mail_body=f.read()
            self.msg=MIMEMultipart()
            att=MIMEText(mail_body,'plain','utf-8')
            att['Content-Type']='application/octet-stream'
            att['Content-Disposition']='attachment;filename=result.html'
            self.msg.attach(att)
            content='自动化测试结果'
            self.msg.attach(MIMEText(content,'plain','utf-8'))
            self.msg['Subject']=Header(self.subject,'utf-8')
            self.msg['From']=self.sender
            self.msg['To']=self.receiver
    def send(self,file):
        self.__config(file)
        try:
            s=smtplib.SMTP()
            s.connect(self.email_server)
            s.login(self.sender,self.password)
            s.sendmail(self.sender,self.receiver,self.msg.as_string())
        except Exception as msg:
            print('邮件发送失败！%s' % msg)
        else:
            print('邮件发送成功!')
        finally:
            s.quit()
if __name__ == '__main__':
    se=SendEmail()
    file='view-source_https___wanandroid.com.html'
    se.send(file)