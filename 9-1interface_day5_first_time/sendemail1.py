"""
功能描述：实现邮件的配置，和对外提供发送邮件的方法
编写人：liuyalin
编写时间：2023/2/6 12:18
实现逻辑：
导包
os
smtplib
time
email.mime.multipart  MIMEMultipart  可以同时添加正文和附件
email.mime.text  MIMEText  实现支持HTML格式的邮件
email.mime.header Header
1. 创建类SendEmail
2. 定义初始化方法，设置初始化属性
发件邮箱服务器email_server
发件邮箱sender
发件邮箱登录用户名user
发件邮箱密码password，填写pop3的授权码
收件邮箱receiver
定义时间格式
邮件主题subject
3. 定义配置邮件的方法私有方法：准备附件，添加附件，组装邮件内容和标题，中文参数需要'utf-8'，单字节不需要
准备附件：
打开html文件，文件对象f
读取附件中的所有内容保存到mail_body,文件对象f调用read()方法
创建邮件对象self.msg，实例化MIMEMultipart类，生成包括多个部分的邮件体，可以同时添加正文和附件的msg
创建附件对象att，实例化MIMEText实现支持HTML格式的邮件
att附件对象添加附件内容
att附件对象添加Content-type   att["Content-Type"] = 'application/octet-stream'
att附件对象添加Content-Disposition   att['Content-Disposition'] = 'attachment;filename = result.html'
将附件添加到邮件内容当中
添加邮件的文本内容：
    准备正文content
    邮件添加正文
    邮件添加主题
    邮件添加发件人
    邮件添加收件人
4. 定义发送方法提供对外方法
配置邮件：调用配置邮件私有方法
登录并发送邮件：
    创建发送邮件的对象s，实例化SMTP类
    连接smtp服务器
    登录邮箱（传参：登录用户名，密码）
    发送邮件（传参：发件人，收件人，邮件内容）
"""


class SendEmail():
    def __init__(self):
        self.email_server = 'smtp.qq.com'
        self.sender = '245555126@qq.com'
        self.password = 'fbkwxhqtoqrrcbed'
        self.receiver = '17600409307@163.com'
        self.subject = ''


# 导包
# os
# smtplib
# time
# email.mime.multipart  MIMEMultipart  可以同时添加正文和附件
# email.mime.text  MIMEText  实现支持HTML格式的邮件
# email.mime.header Header
import os, smtplib, time
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.header import Header


# 1. 创建类SendEmail
class SendEmail():
    # 2. 定义初始化方法，设置初始化属性
    def __init__(self):
        # 发件邮箱服务器email_server
        self.email_server = 'smtp.qq.com'
        # 发件邮箱sender
        self.sender = '245555126@qq.com'
        # 发件邮箱登录用户名user
        self.user = '245555126@qq.com'
        # 发件邮箱密码password，填写pop3的授权码
        self.password = 'fbkwxhqtoqrrcbed'
        # 收件邮箱receiver
        self.receiver = '17600409307@163.com'
        # 定义时间格式
        t = time.strftime('%Y-%m-%d_%H-%M-%S', time.localtime())
        # 邮件主题subject
        self.subject = '自动化测试结果' + t

    # 3. 定义配置邮件的方法私有方法：准备附件，添加附件，组装邮件内容和标题，中文参数需要'utf-8'，单字节不需要
    def __config(self, file):
        # 准备附件：
        # 打开html文件，文件对象f
        f=open(file, 'r')
        # 读取附件中的所有内容保存到mail_body,文件对象f调用read()方法
        mail_body = f.read()
        # 创建邮件对象self.msg，实例化MIMEMultipart类，生成包括多个部分的邮件体，可以同时添加正文和附件的msg
        self.msg = MIMEMultipart()
        # 创建附件对象att，实例化MIMEText实现支持HTML格式的邮件
        att = MIMEText(mail_body, 'plain', 'utf-8')
        # att附件对象添加附件内容:
        # att附件对象添加Content-type   att["Content-Type"] = 'application/octet-stream'
        att['Content-Type'] = 'application/octet-stream'
        # att附件对象添加Content-Disposition   att['Content-Disposition'] = 'attachment;filename = result.html'
        att['Content-Disposition'] = 'attachment;filename=result.html'
        # 将附件添加到邮件内容当中
        self.msg.attach(att)
        # 添加邮件的文本内容：
        # 准备正文content
        content = '这是第一次使用python邮件发送附件测试哦···'
        # 邮件添加正文
        self.msg.attach(MIMEText(content, 'plain', 'utf-8'))
        # 邮件添加主题
        self.msg['Subject'] = Header(self.subject, 'utf-8')
        # 邮件添加发件人
        self.msg['From'] = self.sender
        # 邮件添加收件人
        self.msg['To'] = self.receiver
        f.close()
# 4. 定义发送方法提供对外方法
    def send(self,file):
        # 配置邮件：调用配置邮件私有方法
        self.__config(file)
        # 登录并发送邮件：
        # 创建发送邮件的对象s，实例化SMTP类
        try:
            s=smtplib.SMTP()
            # 连接smtp服务器
            s.connect(self.email_server)
            # 登录邮箱（传参：登录用户名，密码）
            s.login(self.user,self.password)
            # 发送邮件（传参：发件人，收件人，邮件内容）
            s.sendmail(self.sender,self.receiver,self.msg.as_string())
        except Exception as msg:
            print('邮件发送失败！%s'% msg)
        else:
            print('邮件发送成功！')
        finally:
            s.quit()
if __name__=='__main__':
    file='view-source_https___wanandroid.com.html'
    se=SendEmail()
    se.send(file)
