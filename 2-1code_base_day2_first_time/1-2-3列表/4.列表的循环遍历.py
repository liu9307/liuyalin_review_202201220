"""
功能描述：依次打印列表中的各个数据
编写人：liuyalin
编写时间：2022/12/23 18:34
实现逻辑：
导包
1. 
2. 
3. 
"""
# while
list1=[1,3,'a']
i =0
while i <len(list1):
    print(list1[i])
    i+=1

# for
list1=['a','b','c']
for i in list1:
    print(i)
