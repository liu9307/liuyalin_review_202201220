"""
功能描述：
编写人：liuyalin
编写时间：2022/12/23 18:23
实现逻辑：
导包
1. 
2. 
3. 
"""
# copy浅拷贝
import copy
list1=['a',[1,2]]
list2=list1.copy()
# 新对象和原对象地址不同
print(id(list1))
print(id(list2))
# 新对象和原对象里的可变元素地址相同
print(id(list1[1]))
print(id(list2[1]))
# 原对象里的可变元素发生变化，新对象里的也同时变化
list1[1].append(1)
print(list1)
print(list2)
print(id(list1[1]))
print(id(list2[1]))
# copy模块的deepcopy方法，深拷贝
list3=copy.deepcopy(list1)
print(id(list1))
print(id(list3))
# 完全拷贝了父对象及其子对象，新对象和原对象的可变元素的地址不同
print(id(list1[1]))
print(id(list3[1]))
list1[1].append(1)
print(list1)
print(list3)
print(id(list1[1]))
print(id(list3[1]))