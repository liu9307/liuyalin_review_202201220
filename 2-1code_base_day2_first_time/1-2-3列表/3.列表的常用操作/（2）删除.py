"""
功能描述：
编写人：liuyalin
编写时间：2022/12/23 15:25
实现逻辑：
导包
1. 
2. 
3. 
"""
# del
num_list=[1,2,3]
# del num_list
print(num_list)
del (num_list[0])
print(num_list)
# pop
num_list.pop()
print(num_list)
num_list=[1,2,3]
num_list.pop(1)
print(num_list)
# remove
num_list=[1,2,3,2]
num_list.remove(2)
print(num_list)
