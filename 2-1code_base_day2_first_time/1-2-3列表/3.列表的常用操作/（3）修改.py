"""
功能描述：
编写人：liuyalin
编写时间：2022/12/23 15:42
实现逻辑：
导包
1. 
2. 
3. 
"""
# sort
# 列表数据按字典key的值进行排序
list1=[{'name':'Tom','age':13},{'name':'Lily','age':4},{'name':'xiaoming','age':18}]
list1.sort(key=lambda x:x['age'])
print(list1)
# reverse
list1=[1,33,2]
list1.reverse()
print(list1)
print(list(reversed(list1)))
# sort
list1=[1,33,2]
list1.sort(reverse=True)
print(list1)
name_list = ['Tom', 'Lily', 'Rose']
name_list[0] = 'aaa'
# 结果：['aaa', 'Lily', 'Rose']
print(name_list)
num_list = [1, 5, 2, 3, 6, 8]
num_list.reverse()
# 结果：[8, 6, 3, 2, 5, 1]
print(num_list)
num_list = [1, 5, 2, 3, 6, 8]
num_list.sort()
# 结果：[1, 2, 3, 5, 6, 8]
print(num_list)