"""
功能描述：
编写人：liuyalin
编写时间：2022/12/23 10:43
实现逻辑：
导包
1. 
2. 
3. 
"""
# append（）
# 一个数据
name_list=['Tom','Lily']
name_list.append('Xiaoming')
# ['Tom', 'Lily', 'Xiaoming']
print(name_list)
# 一个序列
name_list=['Tom','Lily']
name_list.append(['xiaoming','xiaohong'])
# ['Tom', 'Lily', ['xiaoming', 'xiaohong']]
print(name_list)
# extend()
# 单个数据
name_list=['Tom','Lily','Rose']
name_list.extend('xiaoming')
# ['Tom', 'Lily', 'Rose', 'x', 'i', 'a', 'o', 'm', 'i', 'n', 'g']
print(name_list)
# 序列数据
name_list=['Tom','Lily','Rose']
name_list.extend(['xiaoming','xiaohong'])
# ['Tom', 'Lily', 'Rose', 'xiaoming', 'xiaohong']
print(name_list)

# insert()
name_list=['Tom','Lily','Rose']
name_list.insert(1,'xiaoming')
# ['Tom', 'xiaoming', 'Lily', 'Rose']
print(name_list)
name_list.extend('a','b')
print(name_list)