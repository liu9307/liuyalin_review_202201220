"""
功能描述：需求：有三个办公室，8位⽼师，8位⽼师随机分配到3个办公室
编写人：liuyalin
编写时间：2022/12/23 18:39
实现逻辑：
导包random
1. 定义办公室列表room_list=[[],[],[]]
2. 循环遍历8个老师
    循环体 追加到room_list[random.randint(0,2)]
3. 打印room_list
"""
import random

room_list=[[],[],[]]
for i in range(0,8):
    room_list[random.randint(0,2)].append(i)
print(room_list)