"""
功能描述：
编写人：liuyalin
编写时间：2022/12/23 23:26
实现逻辑：
导包
1. 
2. 
3. 
"""
# 列表推导式
# 需求：创建⼀个0-10的列表。
list1=[i for i in range(0,11)]
print(list1)
# while
i=0
list1=[]
while i <=10:
    list1.append(i)
    i+=1
print(list1)
# for
list1=[]
for i in range(0,11):
    list1.append(i)
print(list1)
# （2）带if的列表推导式
# 需求：创建0-10的偶数列表
list1=[i for i in range(0,11,2)]
print(list1)
list1=[i for i in range(0,11) if i %2==0]
print(list1)
# （3）多个for循环实现列表推导式
# 需求：创建列表如下：
# # [(1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2)]
list1=[(x,y) for x in range(1,3) for y in range(0,3)]
print(list1)
# 字典推导式
# 1．创建⼀个字典：字典key是1-5数字，value是这个数字的2次⽅。
dict1={i:i**2 for i in range(1,6)}
print(dict1)
# 2．将两个列表合并为⼀个字典
list1 = ['name', 'age', 'gender']
list2 = ['Tom', 20, 'man']
dict1={list1[i]:list2[i] for i in range(len(list1))}
print(dict1)
# 3．提取字典中⽬标数据
counts = {'MBP': 268, 'HP': 125, 'DELL': 201, 'Lenovo': 199, 'acer': 99}
# 需求：提取上述电脑数量⼤于等于200的字典数据
counts={x:y for x,y in counts.items() if y>=200}
print(counts)

# 3.集合推导式
# 需求：创建⼀个集合，数据为下⽅列表的2次⽅。
list1=[1,1,2]
set1={i**2 for i in list1}
print(set1)
