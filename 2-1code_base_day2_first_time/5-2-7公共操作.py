"""
功能描述：
编写人：liuyalin
编写时间：2022/12/23 23:05
实现逻辑：
导包
1. 
2. 
3. 
"""
# 1.运算符
str1='a'
str2='b'
str3=str1+str2
print(str3)
str4='a'+'b'
print(str4)
str1='a'*4
print(str1)
list1=['a',1]
list2=list1*2
print(list2)
tuple1=(1,2,'a')
tuple1=tuple1*3
print(tuple1)
set1={1,'a'}
# set2=set1*12
# print(set2)
print(1 in set1)
dict1={'a':1,'b':2}
# dict2=dict1*2
print('c' in dict1.keys())
# 2.公共方法
print(len(str1))
print(len(list1))
print(len(tuple1))
print(len(set1))
print(len(dict1))
print(min(str1))
print(max(dict1.values()))
for i in range(0,2):
    print(i)
print(list(enumerate(list1,0)))
for i in enumerate(list1):
    print(i)

# 3.容器类型转换
print(tuple(list1))
print(list(tuple1))
print(set(str1))
print(tuple(str1))