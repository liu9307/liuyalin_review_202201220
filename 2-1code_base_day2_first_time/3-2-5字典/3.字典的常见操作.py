"""
功能描述：
编写人：liuyalin
编写时间：2022/12/23 19:47
实现逻辑：
导包
1. 
2. 
3. 
"""
# 1.增加
dict1={'name':'Tom','age':19}
dict1['name']='xiaoming'
print(dict1)
dict1['gender']='男'
print(dict1)
# 2.删除
# del
# del dict1
# print(dict1)
del dict1['gender']
print(dict1)
# pop
pop_tuple=dict1.pop('gender','x')
print(pop_tuple)
# popitem
dict2={}
popitem_tuple=dict1.popitem()
# popitem_tuple=dict2.popitem()
print(popitem_tuple)
# clear
dict1.clear()
print(dict1)
# 查询
# key
dict1={'name':'Tom','age':19}
print(dict1['name'])
# get
print(dict1.get('a','XXXX'))
print(dict1.get('a'))
# keys
print(dict1.keys())  # dict_keys(['name', 'age'])
print(type(dict1.keys()))  # <class 'dict_keys'>
# values
print(dict1.values())
# items
print(dict1.items())
