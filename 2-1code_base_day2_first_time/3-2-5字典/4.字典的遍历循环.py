"""
功能描述：
编写人：liuyalin
编写时间：2022/12/23 22:10
实现逻辑：
导包
1. 
2. 
3. 
"""
# （1）遍历字典的key
dict1={'name':'Tom','age':11}
for i in dict1.keys():
    print(i)
# (2)values
for i in dict1.values():
    print(i)
# (遍历键值对）
for i in dict1.items():
    print(i)
print(len(dict1))
