"""
功能描述：8、有一堆字符串，“welocme to super&Test”，打印出tseT&repus ot ……全部单词原位置反转  #不允许用reverse
编写人：liuyalin
编写时间：2022/12/24 0:19
实现逻辑：
导包
1. 
2. 
3. 
"""
# 切片
str1="welocme to super&Test"
print(str1[::-1])
# pop
str1="welocme to super&Test"
list1=list(str1)
str2=''
for i in range(len(list1)):
    pop_str=list1.pop()
    str2+=pop_str
print(str2)
# 首尾互换
str1="welocme to super&Test"
list1=list(str1)
# list2=list1
str2=''
for i in range(len(list1)//2):
    list1[i],list1[-i-1]=list1[-i-1],list1[i]
print(list1)
str2=''.join(list1)
print(str2)