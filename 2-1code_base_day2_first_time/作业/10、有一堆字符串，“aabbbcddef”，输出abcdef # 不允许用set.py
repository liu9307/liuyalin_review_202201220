"""
功能描述：10、有一堆字符串，“aabbbcddef”，输出abcdef # 不允许用set
编写人：liuyalin
编写时间：2022/12/24 0:40
实现逻辑：
导包
1. 
2. 
3. 
"""
str1="aabbbcddef"
str2=''
for i in str1:
    if i not in str2:
        str2+=i
print(str2)