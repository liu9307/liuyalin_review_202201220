"""
功能描述：7、有一堆字符串，“welocme to super&Test”，打印出superTest，#不能查字符的索引
编写人：liuyalin
编写时间：2022/12/24 0:13
实现逻辑：
导包
1. 用空格分割字符串，组成列表
2. 遍历列表
    用&分割
    判断列表长度大于1
        遍历列表
            拼接成字符串
"""
str1="welocme to super&Test"
list1=str1.split(' ')
print(list1)
for i in list1:
    list2=i.split('&')
    if len(list2)>1:
        str2=''
        for y in list2:
            str2+=y
print(str2)