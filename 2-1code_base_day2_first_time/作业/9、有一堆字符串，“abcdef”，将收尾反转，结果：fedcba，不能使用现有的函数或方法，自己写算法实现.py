"""
功能描述：9、有一堆字符串，“abcdef”，将收尾反转，结果：fedcba，不能使用现有的函数或方法，自己写算法实现
编写人：liuyalin
编写时间：2022/12/24 0:32
实现逻辑：
导包
1. 
2. 
3. 
"""
str1 = 'abcdef'
list1 = list(str1)
print(list1)
for i in range(len(list1) // 2):
    list1[i], list1[-i - 1] = list1[-i - 1], list1[i]
# print(list1)
print(''.join(list1))
