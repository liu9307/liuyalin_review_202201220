"""
功能描述：5、求100以内的质数（只能被1和它本身整除）
编写人：liuyalin
编写时间：2022/12/23 23:54
实现逻辑：
导包
1. 
2. 
3. 
"""
list1=[]
# 外循环控制个数
num=1
while num<=100:
    # 循环体
# 内循环判断是否能其它数整除
# 循环变量第i个
    i=2
    while i<num:
        if num%i==0:
            break
        i+=1
    else:
        list1.append(num)
    num+=1
print(list1)