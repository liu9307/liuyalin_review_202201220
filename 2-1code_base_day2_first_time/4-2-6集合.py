"""
功能描述：
编写人：liuyalin
编写时间：2022/12/23 22:29
实现逻辑：
导包
1. 
2. 
3. 
"""
# 增
# add
set1=set('abc')
print(set1)
set1={'a',}
set1.add('abc')
print(set1)
set1.add(1)
print(set1)
# update
# set1.update(1)  # TypeError: 'int' object is not iterable
set1.update([1,2,'aaa'])
print(set1)
set1.update('abcde')
print(set1)
# 删
# del set1
# pop
pop_content=set1.pop()
print(set1)
print(pop_content)
# remove
set1.remove('abc')
print(set1)
# discard
set1.discard('aaa')
print(set1)
# set1.clear()
print(set1)  # set()
# 查
# in
# not in
print('a' in set1)
print('a' not in set1)
print(len(set1))