"""
功能描述：
编写人：liuyalin
编写时间：2022/12/23 19:24
实现逻辑：
导包
1. 
2. 
3. 
"""
# 查找
# 下标
tuple1=(1,2,['a','b'],{'name':'Tom','age':11})
print(type(tuple1))
print(tuple1[3])
# index
print(tuple1.index(2))
# count
print(tuple1.count(2))
# len
print(len(tuple1))
# copy
tuple2=tuple1.copy()
print(tuple2)