"""
功能描述：提供对外方法获取测试数据文件中的数据
编写人：liuyalin
编写时间：2023/2/7 15:30
实现逻辑：
导包
os
log
readconfig
xlrd
1. 定义类ReadExcel
2. 定义初始化方法
    获取文件路径
    获取文件名
        实例化ReadConfig类
        获取文件名
    组装路径+文件名
    打开文件xlrd.open_workbook(filename,encoding='utf-8')
    获取指定sheet页
    获取最大行数和列数
    获取第一行的值
3. 定义提供对外的方法read
    定义数据列表data=[]
    获取每一行的数据（第一行除外）
    每一个元素与第一行的每个元素组成字典
    追加到数据列表
    返回数据列表
"""
# 导包
# os
# log
# configparser
# xlrd
import os, xlrd
from interface2.common.log import logger
from interface2.common.readConfig import ReadConfig


# 1. 定义类ReadExcel
class ReadExcel():
    # 2. 定义初始化方法
    def __init__(self):
        #     获取文件路径
        self.data_dir = os.path.dirname(os.path.dirname(__file__)) + '//testData//'
        #     获取文件名
        #         实例化ReadConfig类
        self.rc = ReadConfig()
        #         获取文件名
        #     组装路径+文件名
        self.data_name = self.data_dir + self.rc.get_config('data', 'data_name')

#     打开文件xlrd.open_workbook(filename,encoding='utf-8')
        self.open_excel=xlrd.open_workbook(self.data_name)
#     获取指定sheet页
        self.sheet1=self.open_excel.sheet_by_index(0)
#     获取最大行数和列数
        self.max_row=self.sheet1.nrows
        self.max_col=self.sheet1.ncols
#     获取第一行的值
        self.first_row=self.sheet1.row_values(0)
# 3. 定义提供对外的方法read
    def read(self):
#     定义数据列表data=[]
        data=[]
#     获取每一行的数据（第一行除外）
        for i in range(1,self.max_row):
            row_value=self.sheet1.row_values(i)
#     每一个元素与第一行的每个元素组成字典
            dict1={self.first_row[x]:row_value[x] for x in range(len(self.first_row))}
#     追加到数据列表
            data.append(dict1)
#     返回数据列表
        return data
if __name__ == '__main__':
    re=ReadExcel()
    re.read()
    logger.debug('data:%s'%re.read())