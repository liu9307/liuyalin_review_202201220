"""
功能描述：定义log模块，提供调试方法
编写人：liuyalin
编写时间：2023/2/7 14:27
实现逻辑：
导包
os
logging
1. 定义log方法
2. 设置日志记录器名称
3. 设置日志级别
4. 设置日志控制台输出格式
5. 设置日志文件输出格式
6. 创建handler控制台输出,logging.StreamHandler
7. 设置日志输出格式
8. 追加到logger
9. 创建handler文件输出，logging.handlers.RotatingFileHandler
10. 设置日志输出格式
11. 追加到logger
"""
# 导包
# os
# logging
import os,logging,time
from logging.handlers import RotatingFileHandler
# 1. 定义log方法
def log():
# 2. 设置日志记录器名称
    logger=logging.getLogger('interface')
# 3. 设置日志级别
    level=logger.setLevel(logging.DEBUG)
# 4. 设置日志控制台输出格式
    console_format=logging.Formatter('%(name)s日志-日志级别：%(levelname)s-%(module)s模块-第%(lineno)d行-内容：%(message)s')
# 5. 设置日志文件输出格式
    file_format=logging.Formatter('日志名：%(name)s-日志生成时间：%(asctime)s-日志级别：%(levelname)s-文件名：%(filename)s-模块名：%(module)s-行号：%(lineno)d-内容：%(message)s')
# 6. 创建handler控制台输出,logging.StreamHandler
    console_handler=logging.StreamHandler()
# 7. 设置日志输出格式
    console_handler.setFormatter(console_format)
# 8. 追加到logger
    logger.addHandler(console_handler)
    # 准备文件路径和文件名
    log_dir=os.path.dirname(os.path.dirname(__file__))+'//testLog//'
# 9. 创建handler文件输出，logging.handlers.RotatingFileHandler
    file_handler=logging.handlers.RotatingFileHandler(log_dir+'RotatingFile.log',mode='a',maxBytes=100,backupCount=3,encoding='utf-8')
# 10. 设置日志输出格式
    file_handler.setFormatter(file_format)
# 11. 追加到logger
    logger.addHandler(file_handler)
    return logger
logger=log()
if __name__ == '__main__':
    logger.debug('这是日志')