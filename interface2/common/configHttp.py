"""
功能描述：单独处理请求的参数配置，和请求方法的判断，以及响应结果的提取
编写人：liuyalin
编写时间：2023/2/7 23:13
实现逻辑：
导包
requests
logger
1. 定义类ConfigHttp
2. 定义初始化方法，设置初始化属性（传参interfaceUrl,method,value)
self.in...
self.me...
self.va...
3.定义__post请求，返回执行后的结果
__get
__put
__delete
4.定义提供对外方法run
判断接口方式，调用对应方法
"""
import requests
from interface2.common.log import logger


class ConfigHttp():
    def __init__(self, interfaceUrl, method, value):
        self.url = interfaceUrl
        self.method = method
        self.value = eval(value)

    def __post(self):
        try:
            result = requests.post(url=self.url, data=self.value)
        except Exception as msg:
            logger.error('接口请求失败，系统提示：%s' % msg)
        else:
            return result

    def __get(self):
        try:
            result = requests.get(url=self.url, params=self.value)
        except Exception as msg:
            logger.error('接口请求失败，系统提示：%s' % msg)
        else:
            return result

    def __put(self):
        try:
            result = requests.put(url=self.url, data=self.value)
        except Exception as msg:
            logger.error('接口请求失败，系统提示：%s' % msg)
        else:
            return result

    def __delete(self):
        try:
            result = requests.delete(url=self.url, params=self.value)
        except Exception as msg:
            logger.error('接口请求失败，系统提示：%s' % msg)
        else:
            return result
    def run(self):
        if self.method.lower()=='post':
            return self.__post()
        elif self.method.lower()=='get':
            return self.__get()
        elif self.method.lower()=='put':
            return self.__put()
        elif self.method.lower()=='delete':
            return self.__delete()
        else:
            print('未找到匹配的请求方式，请检查！')
if __name__ == '__main__':
    url='https://www.wanandroid.com/user/login'
    method='post'
    value="{'username':'liangchao','password':'123456'}"
    ch=ConfigHttp(url,method,value)
    result=ch.run()
    status=result.json()['errorCode']
    logger.debug('status:%s'%status)
