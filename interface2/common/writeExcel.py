"""
功能描述：提供对外方法，写入excel
编写人：liuyalin
编写时间：2023/2/7 17:33
实现逻辑：
导包
os
xlrd
from xlutils.copy import copy
log
readConfig
1. 定义类WriteExcel
2. 定义初始化方法，设置初始化属性
获取文件路径
获取文件名：实例化ReadConfig，调用get_config方法获取文件名，组装文件名
xlrd打开文件
复制文件对象copy
获取复制的对象sheet1
3. 定义提供对外方法write（传参id,real,status)
self.sheet1.write(id,6,real)
self.sheet1.write(id,7,status)
保存
"""
import os, xlrd
from xlutils.copy import copy
from interface2.common.log import logger
from interface2.common.readConfig import ReadConfig


class WriteExcel():
    def __init__(self):
        self.file_dir = os.path.dirname(os.path.dirname(__file__)) + '//testData//'
        self.rc = ReadConfig()
        self.file_name = self.file_dir + self.rc.get_config('data', 'data_name')
        self.open_file = xlrd.open_workbook(self.file_name)
        self.cb = copy(self.open_file)
        self.cs = self.cb.get_sheet(0)

    def write(self, id, real, status):
        self.cs.write(id, 6, real)
        self.cs.write(id, 7, status)
        self.cb.save(self.file_name)


if __name__ == '__main__':
    we = WriteExcel()
    we.write(1, -1, 'fail')
