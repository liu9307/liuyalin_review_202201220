"""
功能描述：配置邮件，发送邮件
编写人：liuyalin
编写时间：2023/2/8 1:04
实现逻辑：
导包
os
smtplib import SMTP
from email.mime.multpart import MIMEMultipart
from email.mime.text import MIMEText

1. 定义类SendEmail
2. 定义初始化方法，设置初始化属性，配置邮箱属性
邮箱服务器
发件人
邮箱登录用户名
密码
收件人
邮件主题：
时间戳
组装主题
3. __config配置邮箱（file)
打开html文件
读取文件read()保存到mail_body变量
实例化邮箱msg=MIMEMultipart()
创建对象att，实例化MIMEText（mail_body,'plain','utf-8'）添加附件内容
att['Content-Type']='application/octet-stream'
att['Content-Disposition']='attachment;filename=result.html'
msg追加att
添加邮件的文本内容
正文content
msg追加正文
msg['Subject']=Header(self.subject,'utf-8')
from
to
4. 定义提供对外方法sendEmail()
实例化smtplib.SMTP()创建对象s
连接服务器connect
登录login
发送sendmail
"""
import os
import smtplib
import time
from smtplib import SMTP
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from interface2.common.log import logger
from interface2.common.readConfig import ReadConfig
from email.header import Header

rc = ReadConfig()
email_server = rc.get_config('email', 'email_server')
sender = rc.get_config('email', 'sender')
user = rc.get_config('email', 'user')
password = rc.get_config('email', 'password')
receiver = rc.get_config('email', 'receiver')


class SendEmail():
    def __init__(self):
        self.email_server = email_server
        self.sender = sender
        self.user = user
        self.password = password
        self.receiver = receiver
        t = time.strftime('%Y-%m-%d_%H-%M-%S', time.localtime())
        self.subject = '自动化测试结果' + t

    def __config(self, file):
        with open(file, 'rb') as f:
            mail_file = f.read()
            self.msg = MIMEMultipart()
            att = MIMEText(mail_file, 'plain', 'utf-8')
            att['Content-Type'] = 'application/octet-stream'
            att['Content-Disposition'] = 'attachment;filename=result.html'
            self.msg.attach(att)
            self.msg['Subject'] = Header(self.subject, 'utf-8')
            self.msg['From'] = self.sender
            self.msg['To'] = self.receiver

    def send(self, file):
        self.__config(file)
        try:
            s = smtplib.SMTP()
            s.connect(self.email_server)
            s.login(self.user, self.password)
            s.sendmail(self.sender, self.receiver, self.msg.as_string())
        except Exception as msg:
            logger.error('邮件发送失败，系统提示：%s' % msg)
        else:
            print('邮件发送成功！')
        finally:
            s.quit()


if __name__ == '__main__':
    se = SendEmail()
    file_dir = os.path.dirname(os.path.dirname(__file__)) + '//testReport//'
    file = file_dir + '2023-02-07_13-46-02report.html'
    se.send(file)
