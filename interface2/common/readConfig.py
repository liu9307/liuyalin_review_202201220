"""
功能描述：提供对外方法，读取配置文件config.ini中的内容
编写人：liuyalin
编写时间：2023/2/7 15:10
实现逻辑：
导包
os
from configparser import ConfigParser
1. 定义类ReadConfig
2. 定义初始化方法，设置初始化属性
获取文件路径
组装文件名
实例化ConfigParser，创建对象conf
conf读取文件
3. 定义提供对外的方法get_config，传参（section,option='all')
如果给option，返回指定section下的指定option的值，用get(section)
如果不给option,返回section下的所有option键值对,用items(section,option)
"""
import os
from interface2.common.log import logger
from configparser import ConfigParser
class ReadConfig():
    def __init__(self):
        self.file_name=os.path.dirname(os.path.dirname(__file__))+'//config.ini'
        self.conf=ConfigParser()
        self.conf.read(self.file_name,encoding='utf-8')
    def get_config(self,section,option='all'):
        if option=='all':
            return self.conf.items(section)
        else:
            return self.conf.get(section,option)
if __name__ == '__main__':
    rc=ReadConfig()
    result=rc.get_config('data','data_name')
    logger.debug('result:%s'%result)
