"""
功能描述：拿到测试数据，根据接口的请求方式判断进行请求，断言请求的结果
编写人：liuyalin
编写时间：2023/2/8 0:24
实现逻辑：
导包
logger
configHttp
readExcel
writeExcel
from ddt import ddt,data,unpack
1. 获取测试数据
实例化ReadExcel
调用read获取数据保存到data_list变量
2. 定义测试用例类TestCase继承unittest.TestCase类，装饰器@ddt装饰
3. 定义接口测试用例（传参id,interfaceUrl,name,method,value,expect),装饰器@data（*data_list)装饰，装饰器@unpack
实例化ConfigHttp(传参url,method,value)
调用run()方法
获取返回结果的real和status
实例化WriteExcel
调用write(id，real,status)
断言
"""
import unittest
from interface2.common.log import logger
from interface2.common.readConfig import ReadConfig
from interface2.common.readExcel import ReadExcel
from interface2.common.writeExcel import WriteExcel
from interface2.common.configHttp import ConfigHttp
from ddt import ddt, data, unpack

re = ReadExcel()
data_list = re.read()
logger.debug('data_list:%s' % data_list)
we = WriteExcel()

@ddt
class TestCase(unittest.TestCase):
    @data(*data_list)
    @unpack
    def test_a(self, id, interfaceUrl, name, method, value, expect, real, status):
        ch = ConfigHttp(interfaceUrl, method, value)
        result = ch.run()
        json1 = result.json()
        logger.debug('json1:%s' % json1)
        real = json1['errorCode']
        if int(real) == int(expect):
            status = 'success'
        else:
            status = 'fail'

        we.write(id, real, status)
        self.assertEqual(real, int(expect), '预期与实际不符，用例执行失败，请检查！')


if __name__ == '__main__':
    unittest.main(verbosity=0)
