"""
功能描述：搜索测试用例，执行用例，生成报告，发送报告，自动清理报告
编写人：liuyalin
编写时间：2.05
实现逻辑：
导包
unittest.TestLoader.discover
unittest
os
sendemail
1. 定义自动清理报告方法（保留最新的3个）
获取报告路径
获取报告列表
切片[:-2]
遍历删除
2. 创建测试套件
suit=unittest.testloader().discover(filedir,pattern,top_level_dir)
返回suit
3. 创建runner对象，实例化HTMLTestRunner
运行测试套件
"""
import os, time, unittest
from HTMLTestRunner import HTMLTestReportCN

from interface2.common.log import logger
from interface2.common.sendEmail import SendEmail


def auto_clear():
    report_dir = os.path.dirname(__file__) + '//testReport//'
    file_list=os.listdir(report_dir)
    if len(file_list)>3:
        for i in file_list[:-3]:
            os.remove(report_dir+i)

def suit():
    case_dir=os.path.dirname(__file__)+'//testCase//'
    suit=unittest.TestLoader().discover(start_dir=case_dir,pattern='test*.py',top_level_dir=None)
    return suit
if __name__ == '__main__':
    auto_clear()
    suit=suit()
    report_dir=os.path.dirname(__file__)+'//testReport//'
    t=time.strftime('%Y-%m-%d_%H-%M-%S',time.localtime())
    report_name=t+'report.html'
    with open(report_dir+report_name,'wb') as f:
        runner=HTMLTestReportCN(stream=f,verbosity=2)
        runner.run(suit)
    while True:
        time.sleep(1)
        if os.path.exists(report_dir+report_name):
            se=SendEmail()
            se.send(report_dir+report_name)
            break
    logger.info('file:%s'%report_name)