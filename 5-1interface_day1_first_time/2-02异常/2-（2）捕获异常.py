"""
功能描述：
编写人：liuyalin
编写时间：2022/12/27 19:41
实现逻辑：
导包
1. 
2. 
3. 
"""
# 捕获指定异常
try:
    print(1/0)
except ZeroDivisionError as msg:
    print(msg)
# 多个
try:
    print(num)
except (NameError,ZeroDivisionError) as msg:
    print(msg)
try:
    print(lis11)
except Exception as msg:
    print(msg)
else:
    print(True)
finally:
    print('finally')