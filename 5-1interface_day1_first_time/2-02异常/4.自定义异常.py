"""
功能描述：
编写人：liuyalin
编写时间：2022/12/27 20:00
实现逻辑：
导包
1. 
2. 
3. 
"""


class ShortInputError(Exception):
    def __init__(self, length, min_len):
        self.length = length
        self.min_len = min_len
    def __str__(self):
        return f"输入的长度是{self.length},至少输入{self.min_len}"
try:
    a=input('输入密码：')
    if len(a)<3:
        raise ShortInputError(len(a),3)
except Exception as msg:
    print(msg)
else:
    print('输入完成')