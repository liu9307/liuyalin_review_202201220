"""
功能描述：需求：将⼩于房⼦剩余⾯积的家具摆放到房⼦中
编写人：liuyalin
编写时间：2022/12/28 0:09
实现逻辑：
导包
1. 房子类
    房子的特征：
        房子面积
        剩余面积
    行为：
        添加家具
2. 家具类
    家具特征：
        家具面积

3. 
"""
class Funiture():
    def __init__(self,name,area):
        self.name=name
        self.area=area
class House():
    def __init__(self,area):
        self.area=area
        self.free_area=100
        self.funiture_list=[]
    def addFuniture(self,funi_name):
        if funi_name.area<self.free_area:
            self.funiture_list.append(funi_name.name)
            self.free_area-=funi_name.area
        else:
            print('放不下')
    def __str__(self):
        return f"房子总面积{self.area},剩余面积{self.free_area},放置了家具：{self.funiture_list}"
bed=Funiture('床',999)
house1=House(100)
house1.addFuniture(bed)
print(house1)