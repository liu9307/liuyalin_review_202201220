"""
功能描述：
需求主线：
1. 被烤的时间和对应的地⽠状态：
0-3分钟：⽣的
3-5分钟：半⽣不熟
5-8分钟：熟的
超过8分钟：烤糊了
2. 添加的调料：
⽤户可以按⾃⼰的意愿添加调料
编写人：liuyalin
编写时间：2022/12/27 23:35
实现逻辑：
导包
1. 定义类烤地瓜类
        地瓜的属性：
            烤地瓜的时间，
            地瓜的状态，
            放的佐料
        地瓜的行为：
            被烤
                用户给出烤制的时间
            添加佐料
               用户给添加佐料，添加到佐料列表
        显示地瓜信息
3. 创建对象/实例化对象
4. 调用烤方法（传时间，放佐料）
"""
# 1. 定义类烤地瓜类
class SweetPotato():
        # 地瓜的属性：
    def __init__(self):
            # 烤地瓜的时间，
        self.cook_time=0
            # 地瓜的状态，
        self.potata_statu='生的'
            # 放的佐料
        self.condiments_list=[]
        # 地瓜的行为：
            # 被烤
    def cook(self,time):
            # 用户给出烤制的时间
        self.cook_time+=time
        # 0 - 3
        # 分钟：⽣的
        # 3 - 5
        # 分钟：半⽣不熟
        # 5 - 8
        # 分钟：熟的
        # 超过8分钟：烤糊了
        if self.cook_time<3:
            self.potata_statu='生的'
        elif 3<=self.cook_time<5:
            self.potata_statu='半生不熟'
        elif 5<=self.cook_time<=8:
            self.potata_statu='熟的'
        else:
            self.potata_statu='烤糊了'
    def addCondiments(self,comdiment):
        self.condiments_list.append(comdiment)
    def __str__(self):
        return f"烤了{self.cook_time}分钟，地瓜状态{self.potata_statu},放了佐料有：{self.condiments_list}"
digua1=SweetPotato()
digua1.cook(8)
print(digua1)
digua1.addCondiments('盐')
print(digua1)