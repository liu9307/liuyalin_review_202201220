"""
功能描述：
编写人：liuyalin
编写时间：2023/2/15 15:43
实现逻辑：
导包
request
from requests.cookie import RequstsCookieJar
1. 准备接口
2. 准备数据
3. 实例化对象jar
4. 请求接口
5. 返回结果
"""
import requests
from requests.cookies import RequestsCookieJar
url1 = 'https://wanandroid.com//user/login'
url2 = 'https://wanandroid.com//user/lg/userinfo/json'
payload = {'username': '17600409307', 'password': 'LIUyalin123'}
# re1=requests.post(url=url1,data=payload)
jar=RequestsCookieJar()
jar.set('JSESSIONID', '77C7A5585CF26914A2D71B5E8981478C', domain='.wanandroid.com')
re=requests.get(url=url2,cookies=jar)
print(re.status_code)
print(re.text)
print(re.cookies)
