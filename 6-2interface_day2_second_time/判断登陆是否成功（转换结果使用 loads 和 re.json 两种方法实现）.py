"""
功能描述：
请求 wanandroid 的登陆接口并判断登陆是否成功（转换结果使用 loads 和 re.json 两种方法实现）
编写人：liuyalin
编写时间：2023/2/15 0:53
实现逻辑：
导包
requests
json
1. 准备接口
2. 准备数据
3. 请求接口
4. 获取结果
5. 断言
"""
import requests,json
import unittest

# 请求接口
url = 'https://wanandroid.com/user/login'
# 请求参数
payload = {'username': '17600409307', 'password': 'LIUyalin123'}
# 请求接口
re = requests.post(url=url, data=payload)
# 打印结果
print(re.text)
# result=json.loads(re.text)
result=re.json()
print(result)
print(result['data']['id'])
