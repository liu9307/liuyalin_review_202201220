"""
功能描述：
接口文档内容：
请求域名：https://www.kuaidi100.com
请求方式：get
请求地址：/query
请求参数：
type ：任意快递名称的全拼，比如：shunfeng
postid：对应的快递单号，可以是无效的
编写人：liuyalin
编写时间：2023/2/14 18:13
实现逻辑：
导包
requests
1. 准备接口
2. 准备数据
3. 携带数据请求接口
"""
import requests
url='https://www.kuaidi100.com/query'
payload={'type':'shunfeng','postid':'SF1409812547112'}
re=requests.get(url=url,params=payload)
print(re.status_code)
print(re.text)
print(re.json())
print(re.elapsed)