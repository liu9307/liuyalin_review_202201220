"""
功能描述：
编写人：liuyalin
编写时间：2023/2/6 22:51
实现逻辑：
导包
log
os
configparser
1. 创建ReadConfig类
2. 定义初始化方法，定义初始化属性
获取config.ini文件路径
读的模式打开文件
3. 定义提供对外方法read(传参section,option='all')
    如果option没有传值，返回所有section下的所有键值对
    如果option传值，返回section下的这个option的值
    返回数据
"""
import os
from interface_1.common.log import logger
from configparser import ConfigParser


class ReadConfig():
    def __init__(self):
        self.file_path = os.path.dirname(os.path.dirname(__file__))
        self.file_name = self.file_path + '//config.ini'
        self.conf = ConfigParser()
        self.conf.read(self.file_name, encoding='utf-8')

    def readConfig(self, section, option='all'):
        if option == 'all':
            return self.conf.items(section)
        else:
            return self.conf.get(section, option)


if __name__ == '__main__':
    rc = ReadConfig()
    rc.readConfig('mysql')
    logger.info('mysql里的option键值对：%s' % rc.readConfig('mysql'))
    logger.info('mysql里的指定option的值：%s' % rc.readConfig('mysql', 'host'))
