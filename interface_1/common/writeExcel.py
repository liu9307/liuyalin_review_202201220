"""
功能描述：请求实际结果和状态写入excel
编写人：liuyalin
编写时间：2023/2/6 22:24
实现逻辑：
导包
os
xlrd
from xlutils.copy import copy
log
1. 创建类WriteExcel
2. 定义初始化属性（传入real,status)
获取文件路径
组装文件名
打开文件
复制文件对象
获取复制对象的sheet
3. 定义提供对外的方法writeData(传入id,real,status)
sheet页.write(id,6,real)
sheet页.write(id,7,status)
复制的文件对象.save(保存文件名含路径）
"""
import os
import xlrd
from xlutils.copy import copy
from interface_1.common.log import logger
import time


class WriteExcel():
    def __init__(self):
        self.file_path = os.path.dirname(os.path.dirname(__file__)) + '//testData//'
        self.file_name = self.file_path + 'data.xls'
        self.wb = xlrd.open_workbook(self.file_name)
        self.cb = copy(self.wb)
        self.cs = self.cb.get_sheet(0)

    def writeData(self, id, real, status):
        self.cs.write(id, 6, real)
        self.cs.write(id, 7, status)
        self.cb.save(self.file_name)


if __name__ == '__main__':
    we = WriteExcel()
    we.writeData(1, 0, 'ok')
