"""
功能描述：单独处理请求的参数配置，和请求方法的判断，以及响应结果的提取
编写人：liuyalin
编写时间：2023/2/6 17:02
实现逻辑：
导包
os
from log import logger
1. 创建类ConfigHttp
2. 定义初始化方法，定义初始化属性（id,interfaceUrl,name,value,method,real,status)
3. 根据接口的请求方式进行判断
4. 返回接口执行后的结果
"""
import os

from interface_1.common import readExcel
from interface_1.common.log import logger
import requests


class ConfigHttp():
    def __init__(self, interfaceUrl, method, value):
        self.interfaceUrl = interfaceUrl
        self.method = method
        self.value = eval(value)

    def __get(self):
        try:
            re = requests.get(url=self.interfaceUrl, params=self.value)
        except Exception as msg:
            logger.error('接口请求失败!系统提示：%s' % msg)
        else:
            return re

    def __post(self):
        try:
            re = requests.post(url=self.interfaceUrl, data=self.value)
        except Exception as msg:
            logger.error('接口请求失败!系统提示：%s' % msg)
        else:
            return re

    def __put(self):
        try:
            re = requests.put(url=self.interfaceUrl, data=self.value)
        except Exception as msg:
            logger.error('接口请求失败！系统提示：%s' % msg)
        else:
            return re

    def __delete(self):
        try:
            re = requests.delete(url=self.interfaceUrl, params=self.value)
        except Exception as msg:
            logger.error('接口请求失败！系统提示：%s' % msg)
        else:
            return re

    def run(self):
        if self.method.lower() == 'get':
            return self.__get()
        elif self.method.lower() == 'post':
            return self.__post()
        elif self.method.lower() == 'put':
            return self.__put()
        elif self.method.lower() == 'delete':
            return self.__delete()
        else:
            logger.error('没有匹配到请求方式，请检查！')


if __name__ == '__main__':
    read_excel = readExcel.ReadExcel()
    data = read_excel.read()
    url = data[0]['interfaceUrl']
    method = data[0]['method']
    value = data[0]['value']
    ch = ConfigHttp(url, method, value)
    ch.run()
    logger.info('请求结束，结果：%s' % ch.run().text)
