"""
功能描述：实现邮件的配置，和对外提供发送邮件的方法
编写人：liuyalin
编写时间：2023/2/6 23:33
实现逻辑：
导包
os
1. 定义类
2. 定义初始化方法
邮箱服务器
发件人邮箱
发件人登录名
密码
收件人
定义时间格式
邮件标题+时间格式
3. 定义私有方法，配置邮件
打开附件
获取附件内容存放mail_body
实例化MIMEMultipart为msg
实例化MIMEText为att,添加mail_body
att添加content-type=application/octet-stream
att添加content-Disposition=attachment;filename=result.html
msg追加att
邮件添加文本内容：
定义content文本内容
邮件追加content   msg.attach(MIMEText(content,'plain','utf-8')
邮件设置发件人
邮件设置收件人
4.定义提供对外方法send
实例化smtplib.SMTP()
连接服务器
登录
发送（传发件人，收件人，msg.as_string)
"""
import os
import smtplib

from interface_1.common.log import logger
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
import time
class SendEmail():
    def __init__(self):
        self.email_server='smtp.qq.com'
        self.sender='245555126@qq.com'
        self.password='fbkwxhqtoqrrcbed'
        self.receiver='17600409307@163.com'
        cur_time=time.strftime('%Y-%m-%d_%H-%M-%S',time.localtime())
        self.subject='接口自动化测试报告'+cur_time
    def __config(self,file):
        # f=open(file,'r',encoding='utf-8')
        # mail_body=f.read()
        # self.msg=MIMEMultipart()
        # att=MIMEText(mail_body,'plain','utf-8')
        # att['Content-Type']='application/octet-stream'
        # att['Content-Disposition']='attachment;filename=result.html'
        # self.msg.attach(att)
        # content='接口自动化测试执行完成，附件是测试用例执行详情，详细内容请查看附件。'
        # self.msg.attach(MIMEText(content,'plain','utf-8'))
        # self.msg['Subject']=Header(self.subject,'utf-8')
        # self.msg['From']=self.sender
        # self.msg['To']=self.receiver
        # f.close()
        with open(file, 'rb') as f:
            mail_body = f.read()
            # 组装邮件内容和标题
            self.msg = MIMEMultipart()
            # 添加附件内容
            att = MIMEText(mail_body, 'plain', 'utf-8')
            att["Content-Type"] = 'application/octet-stream'
            att['Content-Disposition'] = 'attachment;filename = result.html'
            self.msg.attach(att)
            # 添加邮件的文本内容
            content = '这是第一次使用Python 邮件发送附件测试哦...'
            self.msg.attach(MIMEText(content, 'plain', 'utf-8'))
            self.msg['Subject'] = Header(self.subject, 'utf-8')
            self.msg['From'] = self.sender
            self.msg['To'] = self.receiver
    def send(self,file):
        self.__config(file)
        try:
            s=smtplib.SMTP()
            s.connect(self.email_server)
            s.login(self.sender,self.password)
            s.sendmail(self.sender, self.receiver, self.msg.as_string())
        except Exception as msg:
            print('邮件发送失败！%s'%msg)
        else:
            print('邮件发送成功！')
        finally:
            s.quit()

if __name__ == '__main__':
    sm=SendEmail()
    dir1=os.path.dirname(os.path.dirname(__file__))+'//testReport//'
    list1=os.listdir(dir1)
    file=list1[-2]
    print(file)
    sm.send(dir1+file)