"""
功能描述：读取数据文件中的数据
编写人：liuyalin
编写时间：2023/2/6 18:55
实现逻辑：
导包
os
xlrd
1. 定义类ReadExcel
2. 定义初始化方法，定义初始化属性
获取测试数据文件路径
组装数据文件名
创建文件对象打开文件
获取指定sheet页
获取最大行数/列数
获取第一行的值作为字典的key
3. 定义提供对外的方法read
预设空列表data
读取文件中的每一行(除第一行外）for i in range(1,self.max_row):
    每一行的值与第一行的值组成字典
    字典追加到列表
return返回列表
"""
import os
import xlrd
from interface_1.common.log import logger
class ReadExcel():
    def __init__(self):
        self.file_path=os.path.dirname(os.path.dirname(__file__))+'//testData//'
        self.file_name=self.file_path+'data.xls'
        self.readbook=xlrd.open_workbook(self.file_name)
        self.sheet=self.readbook.sheet_by_index(0)
        self.max_row=self.sheet.nrows
        self.max_col=self.sheet.ncols
        self.first_line=self.sheet.row_values(0)
    def read(self):
        data=[]
        for i in range(1,self.max_row):
            row_value=self.sheet.row_values(i)
            dict1={self.first_line[j]:row_value[j] for j in range(len(row_value))}
            logger.debug(dict1)
            data.append(dict1)
        return data
if __name__ == '__main__':
    re=ReadExcel()
    logger.info(re.read())
