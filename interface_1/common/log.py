"""
功能描述：
编写人：liuyalin
编写时间：2023/2/6 19:56
实现逻辑：
导包
os
logging.handlers
1.定义Log类
2. 定义初始化方法，定义初始化属性
设置日志记录器名称
日志级别
日志格式
3. 提供对外方法logger
控制台输出：
创建handler对象h1
设置日志输出格式
logger.addhandler(h1)
创建handler对象h2
设置日志文件存放位置
时间戳
设置日志文件名+时间戳
日志设置输出格式
logger.addhandler添加h2

"""
import os
import time
import logging
import logging.handlers
def log():
    logger=logging.getLogger('Interface')
    level=logger.setLevel(logging.DEBUG)
    format=logging.Formatter('日志记录器名称：%(name)s-文件名：%(filename)s-日志级别:%(levelname)s-模块名：%(module)s-行号：%(lineno)d-日志生成时间：%(asctime)s-日志内容：%(message)s')
    h1=logging.StreamHandler()
    h1.setFormatter(format)
    logger.addHandler(h1)
    file_path=os.path.dirname(os.path.dirname(__file__))+'//testLog//'
    cur_time=time.strftime('%Y-%m-%d_%H-%M-%S',time.localtime())
    h2=logging.handlers.TimedRotatingFileHandler(file_path+'timeRotatingFile.log',when='S',interval=2,backupCount=3,encoding='utf-8')
    h2.setFormatter(format)
    logger.addHandler(h2)
    return logger
logger=log()
if __name__ == '__main__':

    logger.info('这是日志')