"""
功能描述：查找用例，执行用例，生成报告，发送邮件，自动清理报告
编写人：liuyalin
编写时间：2023/2/6 16:59
实现逻辑：
导包
logger
os
time
1. 查找测试用例，生成suite
unittest.testLoader.discover()
2. 执行测试用例，并生成报告
实例化HTMLTestRunner类
调用run(suite)
3. 自动清理报告
方案一：每次运行框架前全部删除
使用os.listdir获取所有文件名列表
遍历列表：
os.remove()

方案二：保留最新的n个，eg:3
获取目录中的所有文件
要删除的=文件列表[:-3]
遍历删除文件

方案三：列表中的报告不是按时间顺序展示时
获取目录中的文件
获取文件时间，组成列表
时间列表为key，文件名为value，组成字典
时间列表按升序排序
取排序后的列表[:-2]删除，就是保留最新的2个，再生成报告，执行用例后目录里会有三个报告
删除排序后的列表对应的value

获取报告路径
获取所有报告组成的列表
获取文件生成时间
组成字典，时间是key
字典列表按时间排序
遍历时间列表到倒数第三个：
    目录移除对应的文件名
"""
import os
import time
from interface_1.common.log import logger
# from interface_1.common.sendEmail import sendEmail
import unittest
from HTMLTestRunner import HTMLTestReportCN

from interface_1.common.sendEmail import SendEmail
# 方案一：
report_dir=os.path.dirname(__file__)+'//testReport//'
report_name = os.listdir(report_dir)
logger.debug('report_dir%s' % report_dir)
def auto_clear1():
    for i in report_name:
        os.remove(report_dir+i)
# 方案二
def auto_clear2():
    for i in report_name[:-2]:
        os.remove(report_dir+i)
# 方案三
def auto_clear3():
    file_time_list=[]
    for i in report_name:
        file_time=os.path.getctime(report_dir+i)
        file_time_list.append(file_time)
    logger.debug('file_time_list:%s'%file_time_list)
    dict1={file_time_list[x]:report_name[x] for x in range(len(file_time_list))}
    logger.debug('dict1:%s'%dict1)
    file_time_list.sort(reverse=True)
    logger.debug('file_time_list:%s'%file_time_list)
    del_time=file_time_list[2:]
    for i in del_time:
        file_name=dict1[i]
        os.remove(report_dir+file_name)

def creatSuite():
    start_dir=os.path.dirname(__file__)+'//testCase//'
    suite=unittest.TestLoader().discover(start_dir,pattern='test*.py',top_level_dir=None)
    return suite

if __name__ == '__main__':
    auto_clear3()
    # 定义报告路径
    # 时间戳
    # 定义报告名=路径+戳+report.html
    # 写入的方式打开
    # 实例化HTMLTestRunner()，创建对象runner
    # suite=creatSuite()创建suite保存到suite变量
    # 调用run(suite)
    # report_dir=os.path.dirname(__file__)+'//testReport//'
    curtime=time.strftime('%Y-%m-%d_%H-%M-%S',time.localtime())
    report_name=report_dir+curtime+'report.html'
    suite=creatSuite()
    with open(report_name,'wb') as f:
        runner=HTMLTestReportCN(stream=f,verbosity=2,title='自动化测试报告',description='测试用例执行情况')
        runner.run(suite)
    # # 判断当前文件是否已经生成，生成后再发送邮件
    # while True:
    #     time.sleep(2)
    #     if os.path.exists(report_name):
    #         sendemail = SendEmail()
    #         sendemail.send(report_name)
    #         break
    # logger.info('file---%s'%report_name)