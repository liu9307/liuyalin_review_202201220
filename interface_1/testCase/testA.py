"""
功能描述：拿到测试数据，根据接口的请求方式判断进行请求，断言请求的结果
编写人：liuyalin
编写时间：2023/2/6 21:39
实现逻辑：
导包
os
log
readExcel
configHttp
from ddt import ddt,data,unpack
1. 获取测试数据
实例化ReadExcel
调用read获取测试数据testdata
取出测试数据中的一条测试用例，进行请求：
2. 创建类TestCase(unittest.TestCase) @ddt
3. 定义测试用例  @data(*testdata)  @unpack
实例化ConfigHttp（传参url,method,value)
调用run方法
re=run()
real=re.json()['errorCode']
self.assertEqual(real,expect)
"""
import os
import unittest
from ddt import ddt,data,unpack
from interface_1.common.log import logger
from interface_1.common.readExcel import ReadExcel
from interface_1.common.configHttp import ConfigHttp
from interface_1.common.writeExcel import WriteExcel
test_data=ReadExcel().read()
@ddt
class TestCase(unittest.TestCase):
    @data(*test_data)
    @unpack
    def test_a(self,id,interfaceUrl,name,method,value,expect,real,status):
        ch=ConfigHttp(interfaceUrl,method,value)
        re=ch.run()
        real=re.json()['errorCode']
        if int(real)==int(expect):
            status='sucess'
        else:
            status='fail'
        we=WriteExcel()
        we.writeData(int(id),real,status)
        self.assertEqual(real,int(expect),'预期与实际不符，用例执行失败，请检查！')
if __name__ == '__main__':
    unittest.main(verbosity=2)