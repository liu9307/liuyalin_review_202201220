"""
功能描述：4.比如有这样一个文件data.txt内存在以下内容（每行采用逗号分隔）
----------------------------
lucy:21,tom:30
xiaoming:18,xiaohong:15
xiaowang:20,xiaohei:19
----------------------------
请通过代码读取文件并输出年龄大于18岁的人名
编写人：liuyalin
编写时间：2022/12/27 0:40
实现逻辑：
导包
打开文件r+模式
定义空列表
1. 循环遍历行数
        读取一行
        用逗号分割
        遍历分割后的列表
            用冒号分割
            分割后的列表，第一个元素给name，第二个元素给age，组成字典，追加到列表
2. 关闭文件
3. 列表中的字典按age排序
"""
f1=open('data.txt','r+',encoding='utf-8')
info=[]
content=f1.readlines()

f1.seek(0)
for i in content:
    i=i.replace('\n','')
    list2=i.split(',')
    print(list2)
    for j in list2:
        list3=j.split(':')
        dict1={'name':list3[0],'age':int(list3[1])}
        info.append(dict1)
print(info)
result=[i for i in info if i['age']>=18]
print(result)
for i in result:
    print(f"{i['name']}大于18")

f1.close()

f=open('data.txt','r')
content=f.readlines()
f.close()
dict1={}
for i in content:
    i=i.replace('\n','')
    list1=i.split(',')
    for j in list1:
        list2=j.split(':')
        dict1[list2[0]]=int(list2[1])
for key,value in dict1.items():
    if value >18:
        print(f"{key}年龄大于18")