"""
功能描述：（七）有一堆字符串，“welocme to super&Test”，打印出emcolew ot  tseT&repus……全部单词原位置反转，
不能使用索引倒序输出或者通过其他现有函数实现，自己写字符串首尾交换方法（15分）
编写人：liuyalin
编写时间：2022/12/27 1:18
实现逻辑：
导包
1. 遍历字符串的一半
2. 首尾互换
3. 
"""
str1="welocme to super&Test"
list1=list(str1)
for i in range(len(str1)//2):
    list1[i],list1[-i-1]=list1[-i-1],list1[i]
print(list1)
print(''.join(list1))

str1="welocme to super&Test"
list1=list(str1)
list2=[]
for i in range(len(list1)):
    str_pop=list1.pop()
    list2.append(str_pop)
print(''.join(list2))

str1="welocme to super&Test"
print(str1[::-1])