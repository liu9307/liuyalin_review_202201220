"""
功能描述：给你一个整数 x ，如果 x 是一个回文整数，返回 true ；否则，返回 false 。

回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。

例如，121 是回文，而 123 不是。

来源：力扣（LeetCode）
链接：https://leetcode.cn/problems/palindrome-number
著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
编写人：liuyalin
编写时间：2023/1/1 0:36
实现逻辑：
导包
1. 转换数据类型为str,结果给变量str1
2. 切片步数-1进行逆置，结果给变量str2
3. 判断，如果str1==str2，返回True
"""

class Solution(object):
    # 方法一：切片
    def isPalindrome(self, x):
        """
        :type x: int
        :rtype: bool
        """
        str1=str(x)
        str2=str1[::-1]
        if str1==str2:
            return True
        else:
            return False
    # 方法二：位置互换
    def isPalindrome2(self,x):
        list1=list(str(x))
        for i in range(list1.count("'")):
            list1.remove("'")
        print(list1)
        for i in range(0,len(list1)//2):
            list1[i],list1[-i-1]=list1[-i-1],list1[i]
        list2=list(str(x))
        if list1==list2:
            return True
        else:
            return False
t1=Solution()
print(t1.isPalindrome2(1211))
print(type(t1.isPalindrome2(1211)))
# int1=123
# list1=[]
# a=f"'{int1}'"
# print(a)
# print(list1)