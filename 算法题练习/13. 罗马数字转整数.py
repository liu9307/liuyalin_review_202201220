"""
功能描述：
罗马数字包含以下七种字符:I，V，X，L，C，D和M。
字符          数值
I             1
V             5
X             10
L             50
C             100
D             500
M             1000
例如， 罗马数字 2 写做II，即为两个并列的 1 。12 写做XII，即为X+II。 27 写做XXVII, 即为XX+V+II。
通常情况下，罗马数字中小的数字在大的数字的右边。但也存在特例，例如 4 不写做IIII，
。数字 1 在数字 5 的左边，所表示的数等于大数 5 减小数 1 得到的数值 4 。同样地，数字 9 表示为IX。
这个特殊的规则只适用于以下六种情况：
I可以放在V(5) 和X(10) 的左边，来表示 4 和 9。
X可以放在L(50) 和C(100) 的左边，来表示 40 和90。
C可以放在D(500) 和M(1000) 的左边，来表示400 和900。
给定一个罗马数字，将其转换成整数。
编写人：liuyalin
编写时间：2023/1/1 1:20
实现逻辑：
导包

1. 遍历给出的罗马数字的每一位：
    判断是否为最后一位：
        加当前值
        判断当前位与下一位的大小，比下一位大或相等，结果加当前值
        否则：
            结果加 下一位减当前值
            i+=1


3. 
"""


def nums(str1):
    if str1 == 'Ⅰ' or str1 == 'I':
        return 1
    elif str1 == 'Ⅴ' or str1 == 'V':
        return 5
    elif str1 == 'Ⅹ' or str1 == 'X':
        return 10
    elif str1 == 'L':
        return 50
    elif str1 == 'C':
        return 100
    elif str1 == 'D':
        return 500
    elif str1 == 'M':
        return 1000
class Solution(object):


    def romanToInt(self, s):
        """
        :type s: str
        :rtype: int
        """
        result=0
        for i in range(len(strn)):
            if i==len(strn)-1:
                num1=nums(strn[i])
                result += num1
                return result
            else:
                num1=nums(strn[i])
                num2=nums(strn[i+1])
                if num1>=num2:
                    result+=num1
                else:
                    result+=num2-num1
                    i+=1
        return result

strn='ⅩⅩⅤⅠⅠ'
t1=Solution()
print(t1.romanToInt(strn))