"""
功能描述：
编写人：liuyalin
编写时间：2022/12/21 16:46
实现逻辑：
导包
1. 
2. 
3. 
"""
# 3.切片
# （3）体验
name = 'abcdefg'
print(name[2:5:1])  # cde
print(name[2:5])  # cde
print(name[:5])  # abcde
print(name[1:])  # bcdefg
print(name[:])  # abcdefg
print(name[::2])  # aceg
print(name[:-1])  # abcdef
print(name[-4:-1])  # def
print(name[::-1])  # gfedcba
print(name[-1:-4:-1])  # gfe 步长是负数时，从序列右边往左边输出
print(name[4:1:-1])  # edc
# 4.字符串的常用操作方法
# 查找
str1 = 'abcdef'
print(str1.find('cde'))
print(str1.find('aa'))
print(str1.rfind('ef'))
print(str1.index('cd'))
# print(str1.index('dd'))
print(str1.rindex('a'))
print(str1.count('d'))
print(len(str1))

# 修改
print(str1.replace('abc', 'aaa'))
print(str1)
print(str1.split('b'))
print(str1)
print(str1.partition('b'))
list1 = ['a', 'b1']
str2 = '-'.join(list1)
print(str2)
print(str1.capitalize())
str1 = '  welcome to BeiJing  '
print(str1.title())
print(str1.upper())
print(str1.lower())
print(str1.rstrip())
print(str1.lstrip())
print(str1.rjust(40))
print(str1.ljust(40,'-'))
print(str1.center(40,'-'))

# 判断
str1 = 'welcometoBeiJing'
print(str1.startswith('we'))
print(str1.endswith('ing'))
print(str1.isalpha())
str1='1a2'
print(str1.isdigit())
print(str1.isalnum())
str1=' '
print(str1.isspace())
