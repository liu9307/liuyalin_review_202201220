"""
功能描述：坐公交，如果有钱可以上车，没钱不能上车；上车后如果有空座，则可以坐下，如果没有空座，就要站着
编写人：liuyalin
编写时间：2022/12/22 18:11
实现逻辑：
导包
假设money=1表示有钱，0表示没钱。seat=1表示有座，0表示没座
1. 接收钱数
2. if判断money==1：
    上车
    接收空座数
    if 空座数==1：
        可以坐下
    else:
        不能坐下
3. else:
        不能上车
"""
money=int(input('输入是否有钱，1表示有钱，0表示没钱：'))
if money==1:
    print('你有钱，可以上车了')
    seat=int(input('输入是否有座位，1表示有座，0表示没座：'))
    if seat==1:
        print('有座位，你可以坐下了')
    else:
        print('没座位，你只能站着了')
else:
    print('你没钱，不能上车了')