"""
功能描述：猜拳游戏
编写人：liuyalin
编写时间：2022/12/22 18:21
实现逻辑：
参与角色：
    玩家（手动出拳）
    电脑（随机出拳）
判断输赢：
    0石头、1剪刀、2布
    平局：玩家==电脑
    玩家胜利：（前面玩家，后面电脑）
            0：1，1：2，2：0
    电脑获胜：（前面玩家，后面电脑）
            0：2，1：0，2：1
导包random
1. 接收玩家出拳player
2. 计算机出拳
3. if 平局：
        输出平局
    elif 玩家胜利：
        输出玩家胜利
    elif 电脑胜利：
        输出电脑胜利
    else:
        输入错误
"""
# 导包
import random

# 玩家出拳
player = int(input('猜拳，0石头、1剪刀、2布，出拳，输入对应数字：'))
# 电脑出拳
computer = random.randint(0, 2)
print(computer)
# 平局
if player == computer:
    print(f'玩家出拳：{player}，电脑出拳{computer}，平局')
# 玩家胜利
elif player == 0 and computer == 1 or (player == 1 and computer == 2) or (player == 2 and computer == 0):
    print(f'玩家出拳{player},电脑出拳{computer}，玩家胜利！')
# 电脑胜利
elif (player == 0 and computer == 2) or (player == 1 and computer == 0) or (player == 2 and computer == 1):
    print(f'玩家出拳{player},电脑出拳{computer},电脑胜利！')
# 其它情况
else:
    print('输入错误')
