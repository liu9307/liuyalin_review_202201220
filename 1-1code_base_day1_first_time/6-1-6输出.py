"""
功能描述：
编写人：liuyalin
编写时间：2022/12/20 17:36
实现逻辑：
导包
1. 
2. 
3. 
"""
# 1.格式化输出
# (1)格式化符号%s字符串，%d整属，%f浮点型
# （2） 体验
age = 18
name = 'TOM'
weight = 75.5
student_id = 1
# 我的名字是TOM
print(f'我的名字是{name}')
print('我的名字是%s' % name)
# 我的学号是0001
print('我的学号是%04d' % student_id)
# 我的体重是75.50公斤
print('我的体重是%.2f公斤' % weight)
# 我的名字是TOM，今年18岁了
print('我的名字是%s，今年%d岁了' % (name, age))
print(f'我的名字是{name}，今年{age}岁了')
# 我的名字是TOM，明年19岁了
print(f'我的名字是{name}，明年{age + 1}岁了')
print('我的名字是%s，明年%d岁了' % (name, age + 1))
# （3）转义字符
print('hello\nworld')
print('hello\tworld')
# 不转义:\\
print('hello\\nworld')
# (4)结束符
print('hello world', end='\t\t\t\t')
print('hello world', end='')