"""
功能描述：三角形右对齐
  *
* *
编写人：liuyalin
编写时间：2022/12/22 21:23
实现逻辑：
导包
方法一：
接收直角边长long
line=1
while line<=long:
' '(long-line),'*'*line
1. 外循环控制行数line
2. 内循环
i=1
while i<=line:
循环体
3. 
"""
long = int(input('直角边长：'))
line = 1
while line <= long:
    i=1
    str1=''
    while i<=line:
        str1+='*'
        i+=1
    print(str1.rjust(long,' '))
    line+=1
