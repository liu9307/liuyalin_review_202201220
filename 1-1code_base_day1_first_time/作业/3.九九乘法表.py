"""
功能描述：
编写人：liuyalin
编写时间：2022/12/22 21:19
实现逻辑：
导包
1. 外循环控制行数
2. 内循环
循环变量打印的第i个
循环条件 i<=line
循环体
循环变量发生变化i+=1
"""
line=1
while line<=9:
    i=1
    while i<=line:
        print(f'{line}×{i}＝{line*i}',end='\t')
        i+=1
    print()
    line+=1
