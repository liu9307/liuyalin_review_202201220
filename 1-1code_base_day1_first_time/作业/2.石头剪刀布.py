"""
功能描述：石头剪刀布
编写人：liuyalin
编写时间：2022/12/22 21:09
实现逻辑：
玩家player
电脑computer
0代表石头，1代表剪刀，2代表布
平局情况：玩家==电脑
玩家胜利：（玩家：电脑）0：1，1：2，2：0
电脑胜利：（电脑：玩家）0：1，1：2，2：0
导包random
1. 接收玩家出拳
2. if 玩家==电脑：
        提示平局
3. elif 玩家胜利情况用or连接：
        提示玩家胜利
4. elif 电脑胜利情况 用or连接：
        提示电脑胜利
5.else：
    提示输入错误
"""
import random
player=int(input('猜拳游戏，0代表石头，1代表剪刀，2代表布，输入对应的数字序号：'))
computer=random.randint(0,2)
if player==computer:
    print(f'玩家出拳：{player}，电脑出拳：{computer}，平局！')
elif (player==0 and computer==1) or (player==1 and computer==2) or (player==2 and computer==0):
    print(f'玩家出拳：{player},电脑出拳：{computer}，玩家胜利！')
elif (computer==0 and player==1) or (computer==1 and player==2) or (computer==2 and player==0):
    print(f'玩家出拳：{player}，电脑出拳：{computer}，电脑胜利！')
else:
    print('输入错误！')