"""
功能描述：1到100累加和
编写人：liuyalin
编写时间：2022/12/22 19:40
实现逻辑：
导包
1. 定义循环变量
    num=1
    result=0
2. while循环条件 num<=100:
    循环语句 result+=num
    循环变量发生变化 num+=1
3. 输出结果result
"""
num=1
result=0
while num<=100:
    result+=num
    num+=1
print(result)
