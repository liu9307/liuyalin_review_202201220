"""
功能描述：
1x1=1
2x1=2  2x2=4
编写人：liuyalin
编写时间：2022/12/22 20:50
实现逻辑：
导包
1. 外循环控制行数
line=1
while line<=9:
    内循环
    line+=1
2. 内循环控制打印当前行第几个
循环变量 i =1
while i<=line:
    print(f"{line}×{i}＝line*i\t',end=''")
    i+=1
print()
"""
line = 1
while line <= 9:
    i = 1
    while i <= line:
        print(f"{line}×{i}＝{line * i}", end='\t')
        i += 1
    print()
    line += 1
