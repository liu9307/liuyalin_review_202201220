"""
功能描述：打印三角形
编写人：liuyalin
编写时间：2022/12/22 20:18
实现逻辑：
导包
1. 外循环控制行数
line=1
while line<=5:
    内循环
    line+=1
2. 内循环控制每行打印的星号数量
i=1
while i<=line:
    print('*  ',end='')
    i+=1
"""
line=1
while line<=5:
    i =1
    while i<=line:
        print('*  ',end='')
        i+=1
    print()
    line+=1