"""
功能描述：
编写人：liuyalin
编写时间：2022/12/22 19:44
实现逻辑：
第一种：循环变量加发生变化时，加2
第二种：判断num是否为偶数：num除以2取余是否为0
导包
1.
2. 
3. 
"""
# 第一种
num=1
result=0
while num<=100:
    if num%2==0:
        result+=num
    num+=1
print(result)
# 第二种
num=2
result=0
while num<=100:
    result+=num
    num+=2
print(result)