"""
功能描述：打印星号（正方形）
编写人：liuyalin
编写时间：2022/12/22 20:03
实现逻辑：
导包
方法一：
1.接收正方形边长long
2.循环变量 行数l=1
3.循环条件 l<=long
3.1    循环体 打印'* '*long
3.2    循环变量发生变化 l+=1
方法二：
外循环控制行数
循环变量 l=1
条件 l<=long:
    循环体 内循环
    循环变量发生变化 l+=1

内循环控制星号数
循环变量第i个星号
循环条件 i <=long:
    循环体 print('*' ,end='')
    循环变量发生变化i+=1
print()
"""
# 方法一
long = int(input("请输入要打印的正方形边长（*）："))
# 方法一
i = 1
while i <= long:
    print('*  ' * long)
    i += 1
# 方法二
print('方法二')
l=1
while l<=long:
    i=1
    while i<=long:
        print('*  ',end='')
        i+=1
    print()
    l+=1