"""
功能描述：
编写人：liuyalin
编写时间：2022/12/20 18:46
实现逻辑：
导包
1. 
2. 
3. 
"""
# 2.转换数据类型的函数：int(),float(),eval(str)用来计算字符串中的有效python表达式并返回一个对象,list(s)将序列转换成列表，tuple(s)将序列s转换为一个元组
# 3.实验
# 转成整型
num1 = 1.1
print(int(num1))
print(type(int(num1)))
# 转成浮点型
num2 = 1
print(float(num2))
print(type(float(num2)))
# eval
str1 = "{'name':'TOM','age':11}"
print(eval(str1))  # {'name': 'TOM', 'age': 11}
print(type(eval(str1)))  # <class 'dict'>
# tuple
list1 = [1, 2]
print(tuple(list1))
print(type(tuple(list1)))
# list
tuple1 = (1, 2)
print(list(tuple1))
print(type(list(tuple1)))
